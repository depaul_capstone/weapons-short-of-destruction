﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestroyOnEnd : MonoBehaviour
{
    void Start()
    {
        float lifetime = 0;
        foreach (ParticleSystem s in GetComponentsInChildren<ParticleSystem>())
        {
            lifetime = (lifetime < s.main.duration) ? s.main.duration : lifetime;
        }
        Destroy(gameObject, lifetime);
    }
}
