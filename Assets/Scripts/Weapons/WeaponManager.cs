﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{

    private static WeaponManager instance = null;
    private static WeaponManager GetInstance()
    {
        return instance;
    }
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
    }

    [SerializeField] GameObject weaponSpawnerPrefab;
    [SerializeField] List<Transform> weaponSpawnLocations;
    private GameObject[] spawners;

    public static void Initialize()
    {
        GetInstance().PrivInitialize();
    }
    private void PrivInitialize()
    {
        List<GameObject> list = new List<GameObject>();
        foreach (Transform spot in weaponSpawnLocations)
        {
            Debug.Log("WeaponManager creating a Spawner");
            GameObject currentSpawner = Instantiate(weaponSpawnerPrefab);
            currentSpawner.transform.position = (spot.position);
            list.Add(currentSpawner);
        }
        spawners = list.ToArray();
    }
}
