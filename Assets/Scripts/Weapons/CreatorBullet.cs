﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class CreatorBullet : BulletBase
{
    [SerializeField] private BulletBase CreateOnDestruction;
    [SerializeField] private GameObject m_Particles;


    void Start()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.Impulse);
    }

    private void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0.0f)
        {
            CreateTornado();
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        lifeTime = 0;
    }
    private void CreateTornado()
    {
        BulletBase t = Instantiate(CreateOnDestruction);
        t.Initialize(shooter, transform);
        GameObject temp = Instantiate(m_Particles, transform.position, Quaternion.identity);
        temp.transform.Rotate(new Vector3(90, 0, 0));
        Destroy(temp, 5);
    }
}
