﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullToTarget : MonoBehaviour
{
    [SerializeField] protected GameObject target = null;
    [SerializeField] protected float pullStrength = 1.0f;
    [SerializeField] protected List<Player> m_caughtPlayers = new List<Player>();
    protected void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            m_caughtPlayers.Add(p);
            if (p.GetController().IsImpulseZeroed())
            {
                p.ResetLastPlayerToHit();
            }
        }
    }

    protected void FixedUpdate()
    {
        foreach (Player p in m_caughtPlayers)
        {
            Vector3 toTarget = target.transform.position - p.transform.position;
            toTarget.Normalize();
            toTarget *= pullStrength;
            p.ApplyImpulseToPlayer(toTarget, null);
        }
    }

    protected void OnTriggerExit(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            RemovePlayer(p);
        }
    }

    public void RemovePlayer(Player p)
    {
        m_caughtPlayers.Remove(p);
    }
    public Player[] GetPlayers()
    {
        return m_caughtPlayers.ToArray();
    }

    public bool ArePlayersCaught()
    {
        return m_caughtPlayers.Count != 0;
    }

    public void SetParentTrap(GameObject t)
    {
        target = t;
    }
}
