﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PullToTargetWithOffset : PullToTarget
{
    protected new void FixedUpdate()
    {
        foreach (Player p in m_caughtPlayers)
        {
            Vector3 toTarget = target.transform.position - p.transform.position;
            toTarget.Normalize();
            toTarget += target.transform.right;
            toTarget *= pullStrength;
            p.ApplyImpulseToPlayer(toTarget, null);
        }
    }
}
