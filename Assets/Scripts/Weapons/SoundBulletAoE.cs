﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBulletAoE : BulletBase, AoEBullet
{
    [SerializeField] protected float pushback;
    public override void Initialize(Player p, Transform t)
    {
        base.Initialize(p, t);
        transform.SetParent(t);
    }

    // Update is called once per frame
    void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0.0f)
        {
            Destroy(gameObject);
        }
    }

    public void OnTriggerStay(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null && p != shooter)
        {
            p.ApplyImpulseToPlayer(transform.TransformDirection(Vector3.forward) * pushback, shooter);
        }
    }
}
