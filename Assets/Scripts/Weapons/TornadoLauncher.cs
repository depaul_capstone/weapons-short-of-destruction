﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoLauncher : WeaponBase
{
    protected override void OnSecondary()
    {
        if (vibrationEndTime < Time.time)
        {
            ChangeState(GunState.NoAmmo);
        }
    }
}
