﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBullet : BulletBase
{
    [SerializeField] protected float pushback;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(transform.InverseTransformDirection(transform.forward) * Time.deltaTime * speed);
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0.0f)
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null && p != shooter)
        {
            p.ApplyImpulseToPlayer(transform.TransformDirection(Vector3.forward) * pushback, shooter);
        }
    }
}
