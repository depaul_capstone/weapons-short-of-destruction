﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using XInputDotNetPure;
public abstract class WeaponBase : MonoBehaviour
{
    public enum GunState
    {
        Idle = 0,
        OnFire = 1,
        OnSecondary = 2,
        OnTriciary = 3,
        NoAmmo = 4,
    }

    static private GunAnimationState[] listOfStates = new GunAnimationState[] { new IdleState(), new FireState(), new SecondaryState(), new TriciaryState(), new NoAmmo() };
    [SerializeField] protected Sprite Reticle;
    [SerializeField] protected Transform BarrelTip;
    [SerializeField] protected BulletBase bulletPrefab;
    [SerializeField] private GunState startingState;

    // sound
    [SerializeField] protected AudioClip pickUpSnd;
    [SerializeField] protected AudioSource pickUpSrc;
    [SerializeField] [Range(0, 1)] protected float pickUpVol;

    [SerializeField] protected AudioClip gunFireSnd;
    [SerializeField] protected AudioSource gunFireSrc;
    [SerializeField] [Range(0, 1)] protected float gunFireVol;

    [SerializeField] protected AnimationCurve vibrationCurve;
    protected float vibrationStartTime = 0;
    protected float vibrationEndTime = 0;

    private GunAnimationState currentState;
    private WeaponHandler handler;

    private void Start()
    {
        ChangeState(startingState);
    }

    public virtual void Initialize(WeaponHandler h, Transform t)
    {
        handler = h;
        transform.SetPositionAndRotation(t.position, t.rotation);
        transform.SetParent(t);
        Reticle ret = handler.GetPlayer().GetReticle();
        ret.changeReticle(Reticle);
        pickUpSrc.volume = pickUpVol;
        SoundManager.PlaySingle(pickUpSnd, pickUpSrc);
    }

    private void Update()
    {
        currentState.run(this);
    }

    protected void ChangeState(GunState state)
    {
        currentState = listOfStates[(int)state];
    }

    protected float PlayerFires()
    {
        Player p = GetPlayer();
        if (p != null)
        {
            return p.GetController().GetFireInput();
        }
        return 0.0f;
    }
    protected Player GetPlayer()
    {
        if (handler != null)
        {
            return handler.GetPlayer();
        }
        return null;
    }

    protected BulletBase CreateBullet()
    {
        BulletBase bullet = Instantiate(bulletPrefab);
        Player holder = GetPlayer();
        bullet.Initialize(holder, BarrelTip);
        return bullet;
    }

    public void SetGunVibration()
    {
        Player p = GetPlayer();
        if (p != null)
        {
            float vibrationPower = vibrationCurve.Evaluate(Time.time - vibrationStartTime);
            p.GetController().SetControllerVibration(vibrationPower);
        }
    }
    public void SetGunVibration(float leftMotor, float rightMotor)
    {
        Player p = GetPlayer();
        if (p != null)
        {
            p.GetController().SetControllerVibration(leftMotor, rightMotor);
        }
    }
    public void SetGunVibration(float bothMotors)
    {
        Player p = GetPlayer();
        if (p != null)
        {
            p.GetController().SetControllerVibration(bothMotors);
        }
    }
    protected virtual void Idle()
    {
        if (PlayerFires() > 0.0f)
        {
            ChangeState(GunState.OnFire);
        }
    }
    protected virtual void OnFire()
    {
        CreateBullet();
        GunVibration();
        gunFireSrc.volume = gunFireVol;
        SoundManager.PlaySingle(gunFireSnd);
        ChangeState(GunState.OnSecondary);
    }
    protected virtual void OnSecondary()
    {
        ChangeState(GunState.OnTriciary);
    }

    protected virtual void OnTriciary()
    {
        ChangeState(GunState.NoAmmo);
    }
    protected void GunVibration()
    {
        vibrationStartTime = Time.time;
        vibrationEndTime = Time.time + vibrationCurve.keys[vibrationCurve.length - 1].time;
        StartCoroutine("VibrationCoroutine");
    }
    private IEnumerator VibrationCoroutine()
    {
        while (Time.time < vibrationEndTime)
        {
            SetGunVibration();
            yield return null;
        }
        SetGunVibration(0.0f);
    }
    protected virtual void OnNoAmmo()
    {
        handler.SetWeaponToNull();
    }
    public void EndProcesses()
    {
        StopAllCoroutines();
        SetGunVibration(0.0f);
    }

    class GunAnimationState
    {
        public virtual void run(WeaponBase Gun) { }
    }

    class FireState : GunAnimationState
    {
        public override void run(WeaponBase Gun)
        {
            Gun.OnFire();
        }
    }
    class SecondaryState : GunAnimationState
    {
        public override void run(WeaponBase Gun)
        {
            Gun.OnSecondary();
        }
    }
    class TriciaryState : GunAnimationState
    {
        public override void run(WeaponBase Gun)
        {
            Gun.OnTriciary();
        }
    }
    class NoAmmo : GunAnimationState
    {
        public override void run(WeaponBase Gun)
        {
            Gun.OnNoAmmo();
        }
    }

    class IdleState : GunAnimationState
    {
        public override void run(WeaponBase Gun)
        {
            Gun.Idle();
        }
    }
}
