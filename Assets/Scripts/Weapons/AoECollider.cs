﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AoECollider : MonoBehaviour
{
    private AoEBullet parent;

    private void Start()
    {
        parent = transform.parent.GetComponent<AoEBullet>();
    }
    private void OnTriggerStay(Collider other)
    {
        parent.OnTriggerStay(other);
    }
}
