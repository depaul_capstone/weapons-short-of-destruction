﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBase : MonoBehaviour
{
    [SerializeField] protected float speed;
    [SerializeField] protected float lifeTime = 0.0f;
    protected Player shooter;

    public virtual void Initialize(Player p, Transform t)
    {
        transform.SetPositionAndRotation(t.position, t.rotation);
        shooter = p;
    }
    public float GetLifetime()
    {
        return lifeTime;
    }
}
