﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornado2 : TornadoBase
{
    [SerializeField] private PullToTarget SuctionArea;

    private void Start()
    {
        transform.rotation = Quaternion.identity;
    }
    private void Update()
    {
        transform.Rotate(transform.up, TornadoRotation);
        if ((lifeTime -= Time.deltaTime) <= 0.0f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            SuctionArea.RemovePlayer(p);
        }
    }
}
