﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tornado1 : TornadoBase
{

    private List<Player> inTrigger = new List<Player>();
    private void Update()
    {
        transform.Rotate(transform.up, 10);
        if ((lifeTime -= Time.deltaTime) <= 0.0f)
        {
            foreach (Player p in inTrigger)
            {
                Vector3 toCenter = p.transform.position - transform.position;
                toCenter += transform.right;
                toCenter.Normalize();
                p.ApplyImpulseToPlayer(toCenter * speed, shooter);
            }
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            inTrigger.Add(p);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            Vector3 toCenter = transform.position - p.transform.position;
            toCenter += transform.right;
            toCenter.Normalize();
            p.ApplyImpulseToPlayer(toCenter * speed, shooter);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            inTrigger.Remove(p);
        }
    }
}
