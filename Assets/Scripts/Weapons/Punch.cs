﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punch : WeaponBase
{
    [SerializeField] private float punchForce;
    [SerializeField] private AnimationClip punchAnimation;
    [SerializeField] private float percentageLengthBeforeNextPunch = 0.5f;
    private bool isPunching = false;
    public override void Initialize(WeaponHandler h, Transform t)
    {
        base.Initialize(h, t);
    }
    protected override void OnFire()
    {
        GetPlayer().GetAnimator().SetTrigger("Punching");
        isPunching = true;
        ChangeState(GunState.OnSecondary);
        float time = punchAnimation.length * percentageLengthBeforeNextPunch;
        Invoke("FinishPunch", time);
    }

    private void FinishPunch()
    {
        ChangeState(GunState.Idle);
        isPunching = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isPunching)
        {
            Player p = other.GetComponent<Player>();
            if (p != GetPlayer() && p != null)
            {
                Reticle r = GetPlayer().GetReticle();
                p.ApplyImpulseToPlayer(r.transform.forward * punchForce, GetPlayer());
            }
        }
    }
}
