﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/****************************************************** 
 * Sound waves are projected out of a bulky cannon. 
 * This weapon is limited in range (about 1 - 3 meters) with an operating cone of 90 degrees horizontally and about 60 degrees vertically. 
 * The power of this weapon decreases in range, but applies a sizeable amount of force against enemies.
 *  A very reliable weapon. (Forward facing cone of force that pushes people away)
 ******************************************************/

public class SoundCannon2_0 : WeaponBase
{
    protected override void OnSecondary()
    {
        if (Time.time > vibrationEndTime)
        {
            ChangeState(GunState.NoAmmo);
        }
    }
}

