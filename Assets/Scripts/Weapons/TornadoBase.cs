﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoBase : BulletBase
{
    [SerializeField] protected float exitSpeed = 0.0f;
    [SerializeField] protected float TornadoRotation = 10.0f;
}
