﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Reticle : MonoBehaviour
{
    public void changeReticle(Sprite newReticle)
    {
        Canvas hud = GetComponentInParent<Canvas>();
        GetComponent<RectTransform>().rect.Set(0, 0, hud.pixelRect.width / 10, hud.pixelRect.height / 10);
        Image ret = GetComponent<Image>();
        if (newReticle != null)
        {
            ret.enabled = true;
            ret.sprite = newReticle;
        }
        else
        {
            ret.enabled = false;
        }
    }
}
