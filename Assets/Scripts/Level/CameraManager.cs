﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{

    private static CameraManager cameraManager = null;
    private static CameraManager GetInstance()
    {
        return cameraManager;
    }
    private void Awake()
    {
        if (cameraManager != null && cameraManager != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            cameraManager = this;
        }
    }

    public enum CameraOrientation
    {
        Horizontal = 0,
        Vertical = 1
    }

    public struct CameraInfo
    {
        public int numPlayers;
        public CameraOrientation screenOrientation; // Only matters if 2 player
    }
    private CameraInfo cameraInfo;

    // Preset 4 Way Split Screen Camera Viewports: TL, TR, BL, BR
    private static Rect[] preset4WaySplit
        = new Rect[] {
            new Rect(0f, 0.5f, 0.5f, 0.5f),
            new Rect(0.5f, 0.5f, 0.5f, 0.5f),
            new Rect(0f, 0f, 0.5f, 0.5f),
            new Rect(0.5f, 0f, 0.5f, 0.5f) };
    // Preset 2 Way Split Screen Camera Viewports: Horizontal(T/B)
    private static Rect[] preset2WaySplitH
        = new Rect[] {
            new Rect(0f, 0.5f, 1f, 0.5f),
            new Rect(0f, 0f, 1f, 0.5f) };
    // Preset 2 Way Split Screen Camera Viewports: Vertical(L/R)
    private static Rect[] preset2WaySplitV
        = new Rect[] {
            new Rect(0f, 0f, 0.5f, 1f),
            new Rect(0.5f, 0f, 0.5f, 1f) };
    // Preset 2 Way Split Screen Camera Viewports Container
    private static Rect[][] preset2WaySplit = new Rect[][] { preset2WaySplitH, preset2WaySplitV };

    private Rect[] currentViewportSettings;
    private Camera[] activeCameras;
    private Canvas[] activeHUDS;
    [SerializeField] Camera defaultFourthCamera;
    [SerializeField] Canvas HUDPrefab;

    public static void Initialize(CameraInfo info)
    {
        GetInstance().PrivInitialize(info);
    }
    private void PrivInitialize(CameraInfo info)
    {
        cameraInfo = info;

        if (cameraInfo.numPlayers == 2)
        {
            currentViewportSettings = preset2WaySplit[(int)cameraInfo.screenOrientation];
            activeCameras = new Camera[2];
        }
        else if (cameraInfo.numPlayers == 1) // For testing purposes only
        {
            Rect[] singlePlayerScreen = new Rect[] { new Rect(0f, 0f, 1f, 1f) };
            currentViewportSettings = singlePlayerScreen;
            activeCameras = new Camera[1];
        }
        else
        {
            currentViewportSettings = preset4WaySplit;
            activeCameras = new Camera[4];
            SetActiveCamera(3, defaultFourthCamera);
        }

        activeHUDS = new Canvas[cameraInfo.numPlayers];
        for (int i = 0; i < activeHUDS.Length; i++)
        {
            activeHUDS[i] = Instantiate(HUDPrefab);
            activeHUDS[i].renderMode = RenderMode.ScreenSpaceCamera;
            activeHUDS[i].pixelPerfect = true;
        }
    }

    public static Rect GetCameraViewportSettings(int playerNum)
    {
        return GetInstance().PrivGetCameraViewportSettings(playerNum);
    }
    private Rect PrivGetCameraViewportSettings(int playerNum)
    {
        return currentViewportSettings[playerNum];
    }

    public static Camera GetActiveCamera(int playerNum)
    {
        return GetInstance().PrivGetActiveCamera(playerNum);
    }
    private Camera PrivGetActiveCamera(int playerNum)
    {
        return activeCameras[playerNum];
    }

    public static Canvas GetActiveHUD(int playerNum)
    {
        return GetInstance().PrivGetActiveHUD(playerNum);
    }
    private Canvas PrivGetActiveHUD(int playerNum)
    {
        return activeHUDS[playerNum];
    }

    public static void SetActiveCamera(int playerNum, Camera cam)
    {
        GetInstance().PrivSetActiveCamera(playerNum, cam);
    }
    private void PrivSetActiveCamera(int playerNum, Camera cam)
    {
        if (activeCameras[playerNum] != null)
        {
            activeCameras[playerNum].enabled = false;
            Debug.Log("Disabled Previous Camera for player" + playerNum);
        }
        cam.enabled = true;
        Debug.Log("Enabled Camera for player" + playerNum);
        cam.rect = GetCameraViewportSettings(playerNum);
        activeCameras[playerNum] = cam;
        if(activeHUDS != null)
        {
            activeHUDS[playerNum].worldCamera = activeCameras[playerNum];
        }
    }
}
