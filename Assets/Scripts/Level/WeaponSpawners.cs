﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSpawners : MonoBehaviour
{
    enum SpawnerStates
    {
        Active = 0,
        Deactive = 1,
    }

    private static BaseSpawnerState[] listOfState = { new ActivateState(), new DeactivateState() };
    [SerializeField] private List<WeaponBase> listOfPossibleWeapons;
    [SerializeField] private float resetTime = 10.0f;
    [SerializeField] private Color activeColor;
    [SerializeField] private Color inactiveColor;
    private float timeLeft;
    private Transform FloatPosition;
    private BaseSpawnerState currentState;
    private WeaponBase CurrentWeapon = null;
    private ParticleSystem[] m_PartSys = null;
    private void ChangeState(SpawnerStates states)
    {
        currentState = listOfState[(int)states];
        currentState.ColorSwap(this);
    }
    private void Awake()
    {
        Transform parent = transform.parent;
        m_PartSys = parent.GetComponentsInChildren<ParticleSystem>();
        FloatPosition = transform.GetChild(0);
    }
    private void Start()
    {
        ChangeState(SpawnerStates.Active);
        timeLeft = resetTime;
    }

    private void Update()
    {
        currentState.run(this);
    }

    private void OnTriggerStay(Collider other)
    {
        currentState.execute(this, other);
    }

    class BaseSpawnerState
    {
        public virtual void run(WeaponSpawners spawner) { }
        public virtual void execute(WeaponSpawners spawner, Collider other) { }
        public virtual void ColorSwap(WeaponSpawners spawner) { }

    }
    class ActivateState : BaseSpawnerState
    {
        public override void execute(WeaponSpawners spawner, Collider other)
        {
            Player p = other.GetComponent<Player>();
            if (p != null)
            {
                p.SetWeapon(spawner.CurrentWeapon);
                spawner.timeLeft = spawner.resetTime;
                spawner.ChangeState(SpawnerStates.Deactive);
                Destroy(spawner.CurrentWeapon.gameObject);
                spawner.CurrentWeapon = null;
            }
        }
        public override void ColorSwap(WeaponSpawners spawner)
        {
            spawner.GetComponent<Renderer>().materials[2].color = spawner.activeColor;
            foreach (ParticleSystem p in spawner.m_PartSys)
            {
                ParticleSystem.MainModule m = p.main;
                Color c = spawner.activeColor;
                c.a = m.startColor.color.a;
                m.startColor = c;
            }
            int i = Random.Range(0, spawner.listOfPossibleWeapons.Count);
            spawner.CurrentWeapon = Instantiate(spawner.listOfPossibleWeapons[i], spawner.transform.parent);
            spawner.CurrentWeapon.transform.SetPositionAndRotation(spawner.FloatPosition.position, spawner.FloatPosition.rotation);
        }
    }

    class DeactivateState : BaseSpawnerState
    {
        public override void run(WeaponSpawners spawner)
        {
            spawner.timeLeft -= Time.deltaTime;
            if (spawner.timeLeft < 0.0f)
            {
                spawner.ChangeState(SpawnerStates.Active);
            }
        }
        public override void ColorSwap(WeaponSpawners spawner)
        {
            spawner.GetComponent<Renderer>().materials[2].color = spawner.inactiveColor;
            foreach (ParticleSystem p in spawner.m_PartSys)
            {
                ParticleSystem.MainModule m = p.main;
                Color c = spawner.inactiveColor;
                c.a = m.startColor.color.a;
                m.startColor = c;
            }
        }
    }
}
