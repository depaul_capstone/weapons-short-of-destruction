﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

    private static ScoreManager scoreManager = null;
    private static ScoreManager GetInstance()
    {
        return scoreManager;
    }
    private void Awake()
    {
        if (scoreManager != null && scoreManager != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            scoreManager = this;
        }
    }

    public enum ScoreState
    {
        InfiniteScore = 0,
        LimitedScore = 1
    }
    static private BaseScoreState[] listOfStates = new BaseScoreState[2] { new InfiniteState(), new LimitedState() };

    public struct ScoreInfo
    {
        public ScoreState state;
        public int maxScore; // Only matters if score is limited
        public int numPlayers;
    }
    private ScoreInfo scoreinfo;

    Text[] playerScoreVisual;

    public static void Initialize(ScoreInfo info)
    {
        GetInstance().PrivInitialize(info);
    }
    private void PrivInitialize(ScoreInfo info)
    {
        scoreinfo = info;
        playerScoreVisual = new Text[scoreinfo.numPlayers];
        for (int i = 0; i < scoreinfo.numPlayers; ++i)
        {
            playerScoreVisual[i] = CameraManager.GetActiveHUD(i).GetComponentsInChildren<Text>()[0];
            playerScoreVisual[i].text = "Score: " + GetPlayerScore(i);
        }

    }

    public static int GetPlayerScore(int playerNum)
    {
        return GetInstance().PrivGetPlayerScore(playerNum);
    }
    private int PrivGetPlayerScore(int playerNum)
    {
        return PlayerManager.GetPlayer(playerNum).GetPlayerScore();
    }

    public static List<Player> GetHighestScoredPlayer()
    {
        return GetInstance().PrivGetHighestScoredPlayer();
    }
    private List<Player> PrivGetHighestScoredPlayer()
    {
        int highestScore = int.MinValue;
        List<Player> highestScoredPlayers = new List<Player>(); // A list in case of a tie
        int playerNum = 0;
        Player currentPlayer = PlayerManager.GetPlayer(playerNum++);

        while (currentPlayer != null)
        {
            if (currentPlayer.GetPlayerScore() > highestScore)
            {
                highestScore = currentPlayer.GetPlayerScore();
                highestScoredPlayers.Clear();
                highestScoredPlayers.Add(currentPlayer);
            }
            else if (currentPlayer.GetPlayerScore() == highestScore)
            {
                highestScoredPlayers.Add(currentPlayer);
            }
            currentPlayer = PlayerManager.GetPlayer(playerNum++);
        }
        return highestScoredPlayers;
    }

    public static void AddToPlayerScore(Player player, int points)
    {
        GetInstance().PrivAddToPlayerScore(player, points);
    }
    private void PrivAddToPlayerScore(Player player, int points)
    {
        player.AddToPlayerScore(points);
        playerScoreVisual[player.GetPlayerNum()].text = "Score: " + GetPlayerScore(player.GetPlayerNum());
        Debug.Log("Player " + player.GetPlayerNum() + " has " + player.GetPlayerScore() + " points");
        listOfStates[(int)scoreinfo.state].run(this, player);
    }

    public static void ResetPlayerScores()
    {
        GetInstance().PrivResetPlayerScores();
    }
    private void PrivResetPlayerScores()
    {
        for (int i = 0; i < playerScoreVisual.Length; ++i)
        {
            playerScoreVisual[i].text = "Score: 0";
            Text t = PlayerManager.GetPlayer(i).GetComponentsInChildren<Text>()[1];
            t.text = "";
        }
    }

    class BaseScoreState
    {
        public virtual void run(ScoreManager manager, Player player) { }
    }

    class InfiniteState : BaseScoreState
    {
        public override void run(ScoreManager manager, Player player)
        {
            // Do nothing
        }
    }

    class LimitedState : BaseScoreState
    {
        public override void run(ScoreManager manager, Player player)
        {
            if (player.GetPlayerScore() >= manager.scoreinfo.maxScore)
            {
                MatchManager.EndMatch(player);
            }
        }
    }
}
