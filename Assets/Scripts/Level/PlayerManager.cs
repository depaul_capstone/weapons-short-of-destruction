﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    private static PlayerManager playerManager = null;
    private static PlayerManager GetInstance()
    {
        return playerManager;
    }
    private void Awake()
    {
        if (playerManager != null && playerManager != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            playerManager = this;
        }
    }

    [SerializeField] GameObject playerPrefab;
    [SerializeField] List<Transform> playerSpawnLocations;
    private GameObject[] players;

    public static void Initialize(int numPlayers, Player.PlayerSettings[] playerSettings)
    {
        GetInstance().PrivInitialize(numPlayers, playerSettings);
    }
    private void PrivInitialize(int numPlayers, Player.PlayerSettings[] playerSettings)
    {
        players = new GameObject[numPlayers];
        CreatePlayers(numPlayers, playerSettings);
        SetMouseLock(true);

        // register 3d listeners on each player
        foreach (GameObject player in players)
        {
            SoundManager.RegisterListener(player.GetComponent<SudoListener>());
        }
    }

    public static void EndMatchCleanUp()
    {
        GetInstance().PrivEndMatchCleanUp();
    }
    private void PrivEndMatchCleanUp()
    {
        // deregister 3d listeners on each player
        foreach (GameObject player in players)
        {
            SoundManager.DeregisterListener(player.GetComponent<SudoListener>());
        }
    }

    private void CreatePlayers(int numPlayers, Player.PlayerSettings[] playerSettings)
    {
        for (int i = 0; i < numPlayers; ++i)
        {
            Debug.Log("PlayerManager creating a player");
            players[i] = Object.Instantiate(playerPrefab);
            players[i].GetComponent<Player>().Initialize(i, playerSettings[i], playerSpawnLocations[i]);
        }
    }

    public static void ResetPlayers()
    {
        GetInstance().PrivResetPlayers();
    }
    private void PrivResetPlayers()
    {
        for (int i = 0; i < players.Length; ++i)
        {
            Debug.Log("PlayerManager resetting a player");
            players[i].GetComponent<Player>().ResetPlayer(playerSpawnLocations[i]);
        }
    }

    public static void UpdateAllPlayerSettings()
    {
        GetInstance().PrivUpdateAllPlayerSettings();
    }
    private void PrivUpdateAllPlayerSettings()
    {
        MatchManager.MatchInfo matchInfo = GlobalGameManager.GetMatchOptions().GetMatchInfo();
        Player.PlayerSettings[] playerSettings = new Player.PlayerSettings[4] {
            matchInfo.player1Settings,
            matchInfo.player2Settings,
            matchInfo.player3Settings,
            matchInfo.player4Settings };
        for (int i = 0; i < players.Length; ++i)
        {
            Debug.Log("PlayerManager updating player settings" + i);
            players[i].GetComponent<Player>().UpdatePlayerSettings(playerSettings[i]);
        }
    }

    public static void KillPlayer(Player player)
    {
        GetInstance().PrivKillPlayer(player);
    }
    private void PrivKillPlayer(Player player)
    {
        Debug.Log("PlayerManager killing a player");
        if (player.GetCurrentState() == Player.PlayerState.Alive)
        {
            player.ChangeState(Player.PlayerState.Dead);
        }
    }

    public static void SetPlayerLock(bool isLocked)
    {
        GetInstance().PrivSetPlayerLock(isLocked);
    }
    private void PrivSetPlayerLock(bool isLocked)
    {
        Debug.Log("Locking Player: " + isLocked);
        SetMouseLock(!isLocked);
        foreach (GameObject player in players)
        {
            player.GetComponent<Player>().GetController().enabled = !isLocked;
        }
    }
    
    private void SetMouseLock(bool isLocked)
    {
        Debug.Log("Locking Mouse: " + isLocked);
        if (isLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public static int GetNumOfPlayers()
    {
        return GetInstance().PrivGetNumOfPlayers();
    }
    private int PrivGetNumOfPlayers()
    {
        return players.Length;
    }

    public static Player GetPlayer(int playerNum)
    {
        return GetInstance().PrivGetPlayer(playerNum);
    }
    private Player PrivGetPlayer(int playerNum)
    {
        if (playerNum >= players.Length)
        {
            return null;
        }
        return players[playerNum].GetComponent<Player>();
    }

    public static Transform GetSpawnLocation()
    {
        return GetInstance().PrivGetSpawnLocation();
    }
    private Transform PrivGetSpawnLocation()
    {
        int spawnPoint = (int)(Random.value * playerSpawnLocations.Count);
        return playerSpawnLocations[spawnPoint];
    }
}
