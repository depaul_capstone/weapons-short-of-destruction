﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchManager : MonoBehaviour
{

    private static MatchManager matchManager = null;
    private static MatchManager GetInstance()
    {
        return matchManager;
    }
    private void Awake()
    {
        if (matchManager != null && matchManager != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            matchManager = this;
        }
    }
    private void Start()
    {
        GlobalGameManager.InitializeMatch();
        Announcer.StartGame();
    }

    public enum MatchState
    {
        Timed = 0,
        Scored = 1,
        Endless = 2,
        Ended = 3
    }
    static private BaseMatchState[] listOfStates = new BaseMatchState[4] { new TimedState(), new ScoredState(), new EndlessState(), new EndedState() };
    private BaseMatchState currentState = listOfStates[(int)MatchState.Endless];

    public struct MatchInfo
    {
        public int numPlayers;
        public CameraManager.CameraOrientation screenOrientation; // Only matters if numPlayers = 2
        public MatchState endCondition;
        public int lengthOfMatch; // Only matters if endCondition = Timed
        public int targetScore; // Only matters if endCondition = Scored
        public Player.PlayerSettings player1Settings;
        public Player.PlayerSettings player2Settings;
        public Player.PlayerSettings player3Settings;
        public Player.PlayerSettings player4Settings;
        // Add as further implementation is added
    }
    private MatchInfo matchInfo;

    [SerializeField] private AudioClip matchMusic = null;
    [SerializeField] int selfKillPenalty = -15;
    private float startTimeOfMatch;

    public static void Initialize(MatchInfo info)
    {
        GetInstance().PrivInitialize(info);
    }
    private void PrivInitialize(MatchInfo info)
    {
        matchInfo = info;
        CameraManager.CameraInfo camInfo = new CameraManager.CameraInfo();
        camInfo.numPlayers = matchInfo.numPlayers;
        camInfo.screenOrientation = matchInfo.screenOrientation;
        CameraManager.Initialize(camInfo);
        PlayerManager.Initialize(matchInfo.numPlayers, 
            new Player.PlayerSettings[4] {
                matchInfo.player1Settings,
                matchInfo.player2Settings,
                matchInfo.player3Settings,
                matchInfo.player4Settings });
        WeaponManager.Initialize();
        ScoreManager.ScoreInfo scoreInfo = new ScoreManager.ScoreInfo();
        if(matchInfo.endCondition == MatchState.Scored)
        {
            scoreInfo.state = ScoreManager.ScoreState.LimitedScore;
        }
        else
        {
            scoreInfo.state = ScoreManager.ScoreState.InfiniteScore;
        }
        scoreInfo.maxScore = matchInfo.targetScore;
        scoreInfo.numPlayers = matchInfo.numPlayers;
        ScoreManager.Initialize(scoreInfo);
        SoundManager.PlayMusic(matchMusic, true);
        currentState = listOfStates[(int)matchInfo.endCondition];
        startTimeOfMatch = Time.time;
    }

    public static void PlayerDeath(Player player, TrapBase trap)
    {
        GetInstance().PrivPlayerDeath(player, trap);
    }
    private void PrivPlayerDeath(Player player, TrapBase trap)
    {
        Debug.Log("Telling Player and Camera Manager about player death");
        CameraManager.SetActiveCamera(player.GetPlayerNum(), trap.GetRandomDeathCamera());
        PlayerManager.KillPlayer(player);
        Player theMurderer = player.GetLastPlayerToHit();
        if (theMurderer == null || theMurderer.GetPlayerNum() == player.GetPlayerNum())
        {
            ScoreManager.AddToPlayerScore(player, selfKillPenalty);
        }
        else
        {
            ScoreManager.AddToPlayerScore(theMurderer, trap.GetTrapKillPoints());
        }
    }

    public static void EndMatch(Player winningPlayer)
    {
        GetInstance().PrivEndMatch(new List<Player>() { winningPlayer });
    }
    public static void EndMatch(List<Player> winningPlayers)
    {
        GetInstance().PrivEndMatch(winningPlayers);
    }
    private void PrivEndMatch(List<Player> winningPlayers)
    {
        Announcer.EndGame();
        Debug.Log("End of Match!!!!!!!!!!!!!!!!!!!");
        if(Cursor.visible == true)
        {
            MenuManager.PreviousMenuState();
        }
        MenuManager.NextMenuState();
        foreach (Player p in winningPlayers)
        {
            Debug.Log("Player" + p.GetPlayerNum() + " won with " + p.GetPlayerScore() + " points!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            GameObject.Find("P" + (p.GetPlayerNum() + 1)).GetComponent<Image>().enabled = true;
            GameObject.Find("P" + (p.GetPlayerNum() + 1) + "Wins").GetComponent<Image>().enabled = true;
        }

        currentState = listOfStates[(int)MatchState.Ended];
    }

    public static void EndMatchCleanUp()
    {
        GetInstance().PrivEndMatchCleanUp();
    }
    private void PrivEndMatchCleanUp()
    {
        PlayerManager.EndMatchCleanUp();
    }

    private void Update()
    {
        currentState.run(this);
    }

    class BaseMatchState
    {
        public virtual void run(MatchManager manager) { }
    }

    class TimedState : BaseMatchState
    {
        public override void run(MatchManager manager)
        {
            if (Time.time - manager.startTimeOfMatch >= manager.matchInfo.lengthOfMatch)
            {
                EndMatch(ScoreManager.GetHighestScoredPlayer());
            }
        }
    }

    class ScoredState : BaseMatchState
    {
        public override void run(MatchManager manager)
        {
            // Do nothing
        }
    }

    class EndedState : BaseMatchState
    {
        public override void run(MatchManager manager)
        {
            // Do nothing
        }
    }

    class EndlessState : BaseMatchState
    {
        public override void run(MatchManager manager)
        {
            // Do nothing
        }
    }
}
