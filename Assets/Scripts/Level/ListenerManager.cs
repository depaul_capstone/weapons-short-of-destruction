﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenerManager : MonoBehaviour
{
    private List<SudoListener> listeners = new List<SudoListener>();
    private List<Sudo3DAudioSource> audioSources = new List<Sudo3DAudioSource>();

    public void UpdateSources()
    {
        foreach (Sudo3DAudioSource source in audioSources)
        {
            ComputeSourceAttributes(source);
        }

    }
    public void RegisterListener(SudoListener listener_)
    {
        listeners.Add(listener_);
    }
    public void DeregisterListener(SudoListener listener_)
    {
        listeners.Remove(listener_);
    }

    public void Register3DAudioSource(Sudo3DAudioSource source_)
    {
        audioSources.Add(source_);
    }

    public void Deregister3DAudioSource(Sudo3DAudioSource source_)
    {
        audioSources.Remove(source_);
    }

    public void ComputeSourceAttributes(Sudo3DAudioSource source_)
    {
        float maxVolume = source_.GetMaxVolume();
        float finalVolume = 0.0f;
        float finalPan = 0.0f;
        foreach (SudoListener listener in listeners)
        {
            float listener_vol = listener.CalculateVolume(source_.GetAudioSource());
            if (listener.CalculateVolume(source_.GetAudioSource()) > finalVolume)
            {
                finalVolume = listener_vol;
            }

            finalPan += listener.CalculatePan(source_.GetAudioSource());
        }

        finalPan /= listeners.Count;


        finalVolume *= maxVolume;
        source_.SetVolume(finalVolume);
        source_.SetPan(finalPan);
    }
}

