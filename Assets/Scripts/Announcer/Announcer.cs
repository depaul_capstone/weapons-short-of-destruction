﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Announcer : MonoBehaviour
{


    [SerializeField] private List<AudioClip> startGame_sndList = new List<AudioClip>();
    [SerializeField] private List<AudioClip> endGame_sndList = new List<AudioClip>();

    private static Announcer instance = null;
    private static Announcer GetInstance()
    {
        return instance;
    }
    private AudioSource AnnouncerSource;
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);

        AnnouncerSource = this.gameObject.AddComponent<AudioSource>();
    }

    public static void StartGame()
    { GetInstance().PrivStartGame(); }
    private void PrivStartGame()
    {
        int randNum = Random.Range(0, startGame_sndList.Count);
        SoundManager.PlaySingle(startGame_sndList[randNum], AnnouncerSource);
    }


    public static void EndGame()
    { GetInstance().PrivEndGame(); }
    private void PrivEndGame()
    {
        int randNum = Random.Range(0, endGame_sndList.Count);
        SoundManager.PlaySingle(endGame_sndList[randNum], AnnouncerSource);
    }
    
    public static void SetAnnouncerVolume(float volume)
    {
        GetInstance().PrivSetAnnouncerVolume(volume);
    }
    private void PrivSetAnnouncerVolume(float volume)
    {
        AnnouncerSource.volume = volume;
    }

    public static float GetAnnouncerVolume()
    {
        return GetInstance().PrivGetAnnouncerVolume();
    }
    private float PrivGetAnnouncerVolume()
    {
        return AnnouncerSource.volume;
    }
}
