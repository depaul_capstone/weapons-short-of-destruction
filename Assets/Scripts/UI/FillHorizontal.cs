﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FillHorizontal : MonoBehaviour
{
    void Update()
    {
        RectTransform self = GetComponent<RectTransform>();
        Rect parent = GetComponentInParent<Canvas>().pixelRect;

        self.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, parent.width);
    }
}
