﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectableButtons : MonoBehaviour
{
    [SerializeField] private Sprite defaultSprite;
    [SerializeField] private Sprite defaultSpriteAlt;
    [SerializeField] private Sprite selectedSprite;
    [SerializeField] private Sprite selectedSpriteAlt;
    [SerializeField] private Sprite disabledSprite;
    [SerializeField] private Sprite disabledSpriteAlt;
    private Button button;
    private bool usingDefaultAlt;
    private bool usingSelectedAlt;
    private bool usingDisabledAlt;

    private void Awake ()
    {
        button = GetComponent<Button>();
	}
    
    public void ToggleDefaultSprite()
    {
        if (usingDefaultAlt)
        {
            button.image.sprite = defaultSprite;
        }
        else
        {
            button.image.sprite = defaultSpriteAlt;
        }
        usingDefaultAlt = !usingDefaultAlt;
    }
    public void UseAltDefaultSprite (bool useAltDefault) {
        if (useAltDefault)
        {
            button.image.sprite = defaultSpriteAlt;
            usingDefaultAlt = true;
        }
        else
        {
            button.image.sprite = defaultSprite;
            usingDefaultAlt = false;
        }
    }

    public void ToggleDisabledSprite()
    {
        SpriteState spriteState = new SpriteState();
        spriteState = button.spriteState;
        if (usingDisabledAlt)
        {
            spriteState.disabledSprite = disabledSpriteAlt;
        }
        else
        {
            spriteState.disabledSprite = disabledSprite;
        }
        button.spriteState = spriteState;
        usingDisabledAlt = !usingDisabledAlt;
    }
    public void UseAltDisabledSprite(bool useAltDisabled)
    {
        SpriteState spriteState = new SpriteState();
        spriteState = button.spriteState;
        if (useAltDisabled)
        {
            spriteState.disabledSprite = disabledSpriteAlt;
            usingDisabledAlt = true;
        }
        else
        {
            spriteState.disabledSprite = disabledSprite;
            usingDisabledAlt = false;
        }
        button.spriteState = spriteState;
    }

    public void ToggleSelectedSprite()
    {
        SpriteState spriteState = new SpriteState();
        spriteState = button.spriteState;
        if (usingSelectedAlt)
        {
            spriteState.highlightedSprite = selectedSpriteAlt;
            spriteState.pressedSprite = selectedSpriteAlt;
        }
        else
        {
            spriteState.highlightedSprite = selectedSprite;
            spriteState.pressedSprite = selectedSprite;
        }
        button.spriteState = spriteState;
        usingSelectedAlt = !usingSelectedAlt;
    }
    public void UseAltSelectedSprite(bool useAltSelected)
    {
        SpriteState spriteState = new SpriteState();
        spriteState = button.spriteState;
        if (useAltSelected)
        {
            spriteState.highlightedSprite = selectedSpriteAlt;
            spriteState.pressedSprite = selectedSpriteAlt;
            usingSelectedAlt = true;
        }
        else
        {
            spriteState.highlightedSprite = selectedSprite;
            spriteState.pressedSprite = selectedSprite;
            usingSelectedAlt = false;
        }
        button.spriteState = spriteState;
    }
}
