﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XInputDotNetPure;

public class CanvasInputs : MonoBehaviour
{
    private static AskForPlayer[] players = new AskForPlayer[] { new Player1(), new Player2(), new Player3(), new Player4(), new AllPlayers() };
    private int PlayerCount;
    private MatchManager.MatchInfo matchInfo;

    private void Start()
    {
        //GlobalGameManager.InitializeGame(this);
        enabled = false;
    }

    public void Initialize(MatchManager.MatchInfo info)
    {
        matchInfo = info;
        PlayerCount = 0;
    }

    void Update()
    {
        CheckForNewPlayer();
    }

    private void CheckForNewPlayer()
    {
        players[PlayerCount].ask(this);
    }
    private void CreateMatch()
    {
        if (PlayerCount > 0)
        {
            MatchManager.Initialize(matchInfo);
            GetComponentInParent<MainCamera>().gameObject.SetActive(false);
        }
        else
        {
            //Change scene to main menu;
            GlobalGameManager.EndMatch();
        }
    }

    abstract class AskForPlayer
    {
        public abstract void ask(CanvasInputs canvas);

    }
    class Player1 : AskForPlayer
    {
        public override void ask(CanvasInputs canvas)
        {
            if (XboxControllerManager.GetButtonPress(PlayerIndex.One, XboxControllerManager.button.A) || Input.GetAxis("Fire1") != 0)
            {
                canvas.PlayerCount++;
                float h = canvas.GetComponent<RectTransform>().rect.height / 4;
                //Activate Bar
                canvas.transform.GetChild(2).gameObject.SetActive(true); //Horizontal Bar

                //Move Player1 Text
                RectTransform player = canvas.transform.GetChild(3).GetComponent<RectTransform>();
                player.localPosition = new Vector2(0, h);
                player.GetComponent<Text>().text = "Player 1\n Ready";
                //Move Player2 Text
                RectTransform player2 = canvas.transform.GetChild(4).GetComponent<RectTransform>();
                player2.gameObject.SetActive(true);
                player2.localPosition = new Vector2(0, -h);
            }
        }
    }
    class Player2 : AskForPlayer
    {
        public override void ask(CanvasInputs canvas)
        {
            if (XboxControllerManager.GetButtonPress(PlayerIndex.Two, XboxControllerManager.button.A))
            {
                canvas.PlayerCount++;
                //Activate Bar
                canvas.transform.GetChild(1).gameObject.SetActive(true); //Virtical Bar


                RectTransform player1 = canvas.transform.GetChild(3).GetComponent<RectTransform>();
                RectTransform player2 = canvas.transform.GetChild(4).GetComponent<RectTransform>();
                RectTransform player3 = canvas.transform.GetChild(5).GetComponent<RectTransform>();

                float y = player1.localPosition.y;
                float x = canvas.GetComponent<RectTransform>().rect.width / 4;

                player1.localPosition = new Vector2(-x, y);
                player2.localPosition = new Vector2(x, y);
                player3.localPosition = new Vector2(-x, -y);
                player3.gameObject.SetActive(true);

                player2.GetComponent<Text>().text = "Player 2\n Ready";
            }
            if (XboxControllerManager.GetButtonPress(PlayerIndex.One, XboxControllerManager.button.Start) || (Input.GetAxis("Submit") != 0))
            {
                canvas.CreateMatch();
            }
        }
    }
    class Player3 : AskForPlayer
    {
        public override void ask(CanvasInputs canvas)
        {
            if (XboxControllerManager.GetButtonPress(PlayerIndex.Three, XboxControllerManager.button.A))
            {
                canvas.PlayerCount++;

                RectTransform player3 = canvas.transform.GetChild(5).GetComponent<RectTransform>();
                RectTransform player4 = canvas.transform.GetChild(6).GetComponent<RectTransform>();

                float y = -player3.localPosition.y;
                float x = -player3.localPosition.x;

                player4.localPosition = new Vector2(x, -y);
                player4.gameObject.SetActive(true);
                player3.GetComponent<Text>().text = "Player 3\n Ready";
            }
            if (XboxControllerManager.GetButtonPress(PlayerIndex.One, XboxControllerManager.button.Start) || (Input.GetAxis("Submit") != 0))
            {
                canvas.CreateMatch();
            }
        }
    }
    class Player4 : AskForPlayer
    {
        public override void ask(CanvasInputs canvas)
        {
            if (XboxControllerManager.GetButtonPress(PlayerIndex.Four, XboxControllerManager.button.A))
            {
                canvas.PlayerCount++;

                RectTransform player4 = canvas.transform.GetChild(6).GetComponent<RectTransform>();
                player4.GetComponent<Text>().text = "Player 4\n Ready";
            }
            if (XboxControllerManager.GetButtonPress(PlayerIndex.One, XboxControllerManager.button.Start) || (Input.GetAxis("Submit") != 0))
            {
                canvas.CreateMatch();
            }
        }
    }
    class AllPlayers : AskForPlayer
    {
        public override void ask(CanvasInputs canvas)
        {
            if (XboxControllerManager.GetButtonPress(PlayerIndex.One, XboxControllerManager.button.Start) || (Input.GetAxis("Submit") != 0))
            {
                canvas.CreateMatch();
            }
        }
    }
}

