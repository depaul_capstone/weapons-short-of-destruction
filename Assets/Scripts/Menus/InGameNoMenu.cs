﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameNoMenu : BaseMenu
{
    public override void Activate(MenuManager manager)
    {
        PlayerManager.SetPlayerLock(false);
        previousState = MenuState.TitleScreen;
        nextState = MenuState.MatchResults;
        manager.RemoveAllMenus();
    }
    public override void Run(MenuManager manager)
    {
        if (Input.GetButtonDown("MenuStart"))
        {
            MenuManager.ChangeState(MenuState.InGameMenu);
        }
    }

    public override void Next(MenuManager manager)
    {
        MenuManager.ChangeState(nextState);
    }
}
