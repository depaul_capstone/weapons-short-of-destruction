﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreenMenu : BaseMenu
{
    public override void Activate(MenuManager manager)
    {
        previousState = MenuState.TitleScreen;
        nextState = MenuState.PlayerSelect;
        manager.ActivateTitleScreenMenu();
    }

    public override void Run(MenuManager manager)
    {
        manager.UpdateSelectedButton();
    }

    public override void Next(MenuManager manager)
    {
        manager.DeactivateTitleScreenMenu();
        MenuManager.ChangeState(nextState);
    }
}
