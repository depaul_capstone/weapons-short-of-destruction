﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchResultsMenu : BaseMenu
{
    public override void Activate(MenuManager manager)
    {
        PlayerManager.SetPlayerLock(true);
        previousState = MenuState.TitleScreen;
        nextState = MenuState.TitleScreen;
        manager.ActivateMatchResultsMenu();
    }
    public override void Run(MenuManager manager)
    {
        manager.UpdateSelectedButton();
    }

    public override void Next(MenuManager manager)
    {
        GlobalGameManager.EndMatch();
        manager.DeactivateMatchResultsMenu();
        MenuManager.ChangeState(nextState);
    }
}
