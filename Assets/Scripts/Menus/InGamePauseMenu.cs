﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGamePauseMenu : BaseMenu
{
    public override void Activate(MenuManager manager)
    {
        PlayerManager.SetPlayerLock(true);
        previousState = MenuState.NoMenu;
        nextState = MenuState.TitleScreen;
        manager.ActivateInGameMenu();
    }
    public override void Run(MenuManager manager)
    {
        manager.UpdateSelectedButton();
        if (Input.GetButtonDown("MenuCancel"))
        {
            Previous(manager);
        }
    }

    public override void Previous(MenuManager manager)
    {
        manager.DeactivateInGameMenu();
        MenuManager.ChangeState(previousState);
    }
    public override void Next(MenuManager manager)
    {
        GlobalGameManager.EndMatch();
        manager.DeactivateInGameMenu();
        MenuManager.ChangeState(nextState);
    }
}
