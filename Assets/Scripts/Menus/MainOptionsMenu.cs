﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainOptionsMenu : BaseMenu
{
    // Bleh...
    /*static protected BaseOptionPage[] listOfPages =
        new BaseOptionPage[5] {
            new SoundOptionPage(),
            new ControlOptionPage(0),
            new ControlOptionPage(1),
            new ControlOptionPage(2),
            new ControlOptionPage(3)
        };      
    protected BaseOptionPage currentPage;

    public BaseOptionPage GetCurrentOptionPage()
    {
        return currentPage;
    }
    public void ChangeOptionPage(MenuManager manager, BaseOptionPage.OptionsPage page)
    {
        currentPage = listOfPages[(int)page];
        currentPage.Activate(manager);
    }*/

    public int GetPlayerMenuNum()
    {
        return currentOptionsPage - 1;
    }

    protected int currentOptionsPage;

    public override void Activate(MenuManager manager)
    {
        currentOptionsPage = 0;
        previousState = MenuState.TitleScreen;
        nextState = MenuState.TitleScreen; // More states will be added during architectural restructuring
        manager.ActivateMainOptionsMenu();
        PopulateSoundOptionsMenu();
    }
    public override void Run(MenuManager manager)
    {
        manager.UpdateSelectedButton();
        if (Input.GetButtonDown("MenuCancel"))
        {
            Previous(manager);
        }
    }

    public override void Previous(MenuManager manager)
    {
        base.Previous(manager);
        // This will probably need to be state based; will fix during architectural restructuring
        if (currentOptionsPage == 0)
        {
            manager.DeactivateMainOptionsMenu();
            MenuManager.ChangeState(previousState);
        }
        else if (currentOptionsPage == 1)
        {
            manager.DeactivateControlOptionsMenu();
            manager.ActivateMainOptionsMenu();
            --currentOptionsPage;
            PopulateSoundOptionsMenu();
        }
        else
        {
            if (currentOptionsPage == 4)
            {
                GameObject.Find("ToNextControlOptions").GetComponent<Button>().interactable = true;
            }
            GameObject.Find("ControlsP" + currentOptionsPage).GetComponent<Image>().enabled = false;
            GameObject.Find("ControlsP" + --currentOptionsPage).GetComponent<Image>().enabled = true;
            PopulateControlOptionsMenu(currentOptionsPage);
        }
    }
    public override void Next(MenuManager manager)
    {
        // This will probably need to be state based; will fix during architectural restructuring
        if (currentOptionsPage == 0)
        {
            manager.DeactivateMainOptionsMenu();
            manager.ActivateControlOptionsMenu();
            ++currentOptionsPage;
        }
        else
        {
            if (currentOptionsPage == 3)
            {
                GameObject.Find("ToNextControlOptions").GetComponent<Button>().interactable = false;
            }
            GameObject.Find("ControlsP" + currentOptionsPage).GetComponent<Image>().enabled = false;
            GameObject.Find("ControlsP" + ++currentOptionsPage).GetComponent<Image>().enabled = true;
        }
        PopulateControlOptionsMenu(currentOptionsPage);
    }

    private void PopulateSoundOptionsMenu()
    {
        GameObject.Find("GameVolumeSlider").GetComponent<Slider>().value = SoundManager.GetGameSoundEffectsVolume();
        GameObject.Find("MusicVolumeSlider").GetComponent<Slider>().value = SoundManager.GetGameMusicVolume();
        GameObject.Find("AnnouncerVolumeSlider").GetComponent<Slider>().value = Announcer.GetAnnouncerVolume();
    }

    private void PopulateControlOptionsMenu(int playerNum)
    {
        MatchManager.MatchInfo matchInfo = GlobalGameManager.GetMatchOptions().GetMatchInfo();
        Player.PlayerSettings playerSettings;
        switch (playerNum)
        {
            case 1:
                playerSettings = matchInfo.player1Settings;
                break;
            case 2:
                playerSettings = matchInfo.player2Settings;
                break;
            case 3:
                playerSettings = matchInfo.player3Settings;
                break;
            case 4:
                playerSettings = matchInfo.player4Settings;
                break;
            default:
                playerSettings = new Player.PlayerSettings();
                Debug.Log("This shouldn't happen - We should only have 4 players?");
                break;
        }
        ShowCamInvertH(playerSettings.invertCamHorizontal);
        ShowCamInvertV(playerSettings.invertCamVertical);
        ShowHSensitivity(playerSettings.horizontalSensitivity);
        ShowVSensitivity(playerSettings.verticalSensitivity);
    }

    private void ShowCamInvertH(bool invert)
    {
        GameObject.Find("InvertCamXSwitch").GetComponent<SelectableButtons>().UseAltDefaultSprite(invert);
    }
    private void ShowCamInvertV(bool invert)
    {
        GameObject.Find("InvertCamYSwitch").GetComponent<SelectableButtons>().UseAltDefaultSprite(invert);
    }

    private void ShowHSensitivity(float sensitivity)
    {
        ShowSensitivity('X', sensitivity);
    }
    private void ShowVSensitivity(float sensitivity)
    {
        ShowSensitivity('Y', sensitivity);
    }

    private void ShowSensitivity(char direction, float sensitivity)
    {
        if (sensitivity == (int)Player.PlayerSensitivityPresets.Least)
        {
            GameObject.Find("S" + direction + "Least").GetComponent<SelectableButtons>().UseAltDefaultSprite(true);
            GameObject.Find("S" + direction + "Some").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Middle").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "More").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Most").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
        }
        else if (sensitivity == (int)Player.PlayerSensitivityPresets.Some)
        {
            GameObject.Find("S" + direction + "Least").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Some").GetComponent<SelectableButtons>().UseAltDefaultSprite(true);
            GameObject.Find("S" + direction + "Middle").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "More").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Most").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
        }
        else if (sensitivity == (int)Player.PlayerSensitivityPresets.Middle)
        {
            GameObject.Find("S" + direction + "Least").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Some").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Middle").GetComponent<SelectableButtons>().UseAltDefaultSprite(true);
            GameObject.Find("S" + direction + "More").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Most").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
        }
        else if (sensitivity == (int)Player.PlayerSensitivityPresets.More)
        {
            GameObject.Find("S" + direction + "Least").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Some").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Middle").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "More").GetComponent<SelectableButtons>().UseAltDefaultSprite(true);
            GameObject.Find("S" + direction + "Most").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
        }
        else if (sensitivity == (int)Player.PlayerSensitivityPresets.Most)
        {
            GameObject.Find("S" + direction + "Least").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Some").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Middle").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "More").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Most").GetComponent<SelectableButtons>().UseAltDefaultSprite(true);
        }
        else
        {
            Debug.Log("This should only happen if no options have been chosen yet - although should probably have defaults set and shown");
            GameObject.Find("S" + direction + "Least").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Some").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Middle").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "More").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
            GameObject.Find("S" + direction + "Most").GetComponent<SelectableButtons>().UseAltDefaultSprite(false);
        }
    }
}
