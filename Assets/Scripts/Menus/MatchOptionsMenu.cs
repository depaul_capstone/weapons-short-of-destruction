﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchOptionsMenu : BaseMenu
{
    public override void Activate(MenuManager manager)
    {
        previousState = MenuState.PlayerSelect;
        nextState = MenuState.TimeOptions;
        manager.ActivateMatchOptionsMenu();
    }
    public override void Run(MenuManager manager)
    {
        manager.UpdateSelectedButton();
        if (Input.GetButtonDown("MenuCancel"))
        {
            Previous(manager);
        }
    }

    public override void Previous(MenuManager manager)
    {
        manager.DeactivateMatchOptionsMenu();
        MenuManager.ChangeState(previousState);
    }
}
