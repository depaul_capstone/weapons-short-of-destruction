﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerSelectMenu : BaseMenu
{
    public enum ControllerStatus
    {
        NotPlaying = 0,
        Playing = 1,
        Ready = 2
    }
    static private BaseControllerState[] listOfStates = new BaseControllerState[3] { new NotPlayingState(), new PlayingState(), new ReadyState() };

    struct PlayerController
    {
        public XInputDotNetPure.PlayerIndex controllerNum;
        public ControllerStatus currentState;
        public KeyCode keyboardTestingA1; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
        public KeyCode keyboardTestingA2; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
    }
    PlayerController[] controllers;
    int playingControllers;
    int readyControllers;
    private const int FRAME_BUFFER = 1;
    private int currentFrame;

    public override void Activate(MenuManager manager)
    {
        controllers = new PlayerController[4];
        controllers[0].controllerNum = XInputDotNetPure.PlayerIndex.One;
        controllers[0].keyboardTestingA1 = KeyCode.Alpha1; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
        controllers[0].keyboardTestingA2 = KeyCode.Keypad1; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
        controllers[1].controllerNum = XInputDotNetPure.PlayerIndex.Two;
        controllers[1].keyboardTestingA1 = KeyCode.Alpha2; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
        controllers[1].keyboardTestingA2 = KeyCode.Keypad2; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
        controllers[2].controllerNum = XInputDotNetPure.PlayerIndex.Three;
        controllers[2].keyboardTestingA1 = KeyCode.Alpha3; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
        controllers[2].keyboardTestingA2 = KeyCode.Keypad3; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
        controllers[3].controllerNum = XInputDotNetPure.PlayerIndex.Four;
        controllers[3].keyboardTestingA1 = KeyCode.Alpha4; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
        controllers[3].keyboardTestingA2 = KeyCode.Keypad4; // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY

        playingControllers = 0;
        readyControllers = 0;
        currentFrame = FRAME_BUFFER;

        previousState = MenuState.TitleScreen;
        nextState = MenuState.MatchOptions;

        manager.ActivatePlayerSelectMenu();
    }

    public override void Run(MenuManager manager)
    {
        manager.UpdateSelectedButton();

        if(currentFrame == 0)
        {
            if (ReturnToPreviousMenu(manager))
            {
                Previous(manager);
                return;
            }

            foreach (PlayerController controller in controllers)
            {
                listOfStates[(int)controller.currentState].Run(this, controller);
            }
        }
        else
        {
            --currentFrame;
        }

        EnableNextMenuAccess(manager);
    }

    public override void Previous(MenuManager manager)
    {
        manager.DeactivatePlayerSelectMenu();
        MenuManager.ChangeState(previousState);
    }
    public override void Next(MenuManager manager)
    {
        Queue<int> playerControllers = new Queue<int>();
        foreach (PlayerController controller in controllers)
        {
            if (controller.currentState == ControllerStatus.Ready)
            {
                playerControllers.Enqueue((int)controller.controllerNum);
            }
        }

        MatchOptions matchOptions = GlobalGameManager.GetMatchOptions();
        int numPlayers = playerControllers.Count;
        matchOptions.SetNumOfPlayers(numPlayers);
        for(int i = 0; i < numPlayers; ++i)
        {
            matchOptions.SetPlayerControllerNum(i, playerControllers.Dequeue());
        }

        manager.DeactivatePlayerSelectMenu();
        MenuManager.ChangeState(nextState);
    }

    private bool ReturnToPreviousMenu(MenuManager manager)
    {
        // If no active Players, return to previous menu
        return Input.GetButtonDown("MenuCancel") && playingControllers == 0;
    }

    private void EnableNextMenuAccess(MenuManager manager)
    {
        // If all Player's Ready, give access to next menu
        if (readyControllers >= 1 && readyControllers == playingControllers) // TODO: CHANGE TO (readyControllers >= 2) FOR END PRODUCT
        {
            GameObject.Find("Players Ready").GetComponent<Button>().interactable = true;
        }
        else
        {
            GameObject.Find("Players Ready").GetComponent<Button>().interactable = false;
        }
    }

    private void ChangeState(int controllerNum, ControllerStatus newState)
    {
        controllers[controllerNum].currentState = newState;
    }

    class BaseControllerState
    {
        public virtual void Run(PlayerSelectMenu playerSelect, PlayerController controller) { }
    }

    class NotPlayingState : BaseControllerState
    {
        public override void Run(PlayerSelectMenu playerSelect, PlayerController controller)
        {
            if(XboxControllerManager.GetButtonPress(controller.controllerNum, XboxControllerManager.button.A) 
                || Input.GetKeyDown(controller.keyboardTestingA1) // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
                || Input.GetKeyDown(controller.keyboardTestingA2)) // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
            {
                MenuManager.PlaySelectSnd();
                playerSelect.ChangeState((int)controller.controllerNum, ControllerStatus.Playing);
                playerSelect.playingControllers++;

                // Swap Visible Image Prompts
                int playerIndexOffset = (int)controller.controllerNum + 1;
                GameObject.Find("PressA" + playerIndexOffset).GetComponent<Image>().enabled = false;
                GameObject.Find("PressStart" + playerIndexOffset).GetComponent<Image>().enabled = true;

                // Drop Player Robot
                GameObject playerRobot = GameObject.Find("Player" + playerIndexOffset);
                playerRobot.GetComponent<MenuRobotAnimations>().EnableRobotMesh(true);
                playerRobot.GetComponent<MenuRobotAnimations>().ChangeState(MenuRobotAnimations.MenuRobotState.GoingDown);
            }
        }
    }

    class PlayingState : BaseControllerState
    {
        public override void Run(PlayerSelectMenu playerSelect, PlayerController controller)
        {
            if (XboxControllerManager.GetButtonPress(controller.controllerNum, XboxControllerManager.button.B)
                || Input.GetKeyDown(KeyCode.Backspace) // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
                || Input.GetKeyDown(KeyCode.Delete)) // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
            {
                MenuManager.PlayBackSnd ();
                playerSelect.ChangeState((int)controller.controllerNum, ControllerStatus.NotPlaying);
                playerSelect.playingControllers--;

                // Swap Visible Image Prompts
                int playerIndexOffset = (int)controller.controllerNum + 1;
                GameObject.Find("PressStart" + playerIndexOffset).GetComponent<Image>().enabled = false;
                GameObject.Find("PressA" + playerIndexOffset).GetComponent<Image>().enabled = true;

                // Lift Player Robot
                GameObject playerRobot = GameObject.Find("Player" + playerIndexOffset);
                playerRobot.GetComponentInChildren<Animator>().SetTrigger("Jumping");
                playerRobot.GetComponent<MenuRobotAnimations>().ChangeState(MenuRobotAnimations.MenuRobotState.GoingUp);
                playerSelect.currentFrame = FRAME_BUFFER;
            }

            if (XboxControllerManager.GetButtonPress(controller.controllerNum, XboxControllerManager.button.Start)
                || Input.GetKeyDown(KeyCode.Space) // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
                || Input.GetKeyDown(KeyCode.Return)) // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
            {
                MenuManager.PlaySelectSnd ();
                playerSelect.ChangeState((int)controller.controllerNum, ControllerStatus.Ready);
                playerSelect.readyControllers++;

                // Swap Visible Image Prompts
                GameObject.Find("PressStart" + ((int)controller.controllerNum + 1)).GetComponent<Image>().enabled = false;
                GameObject.Find("Ready" + ((int)controller.controllerNum + 1)).GetComponent<Image>().enabled = true;
            }
        }
    }

    class ReadyState : BaseControllerState
    {
        public override void Run(PlayerSelectMenu playerSelect, PlayerController controller)
        {
            if (XboxControllerManager.GetButtonPress(controller.controllerNum, XboxControllerManager.button.B)
                || Input.GetKeyDown(KeyCode.Backspace) // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
                || Input.GetKeyDown(KeyCode.Delete)) // TODO: REMOVE FOR END PRODUCT - FOR TESTING ONLY
            {
                MenuManager.PlayBackSnd ();
                playerSelect.ChangeState((int)controller.controllerNum, ControllerStatus.Playing);
                playerSelect.readyControllers--;

                // Swap Visible Image Prompts
                GameObject.Find("Ready" + ((int)controller.controllerNum + 1)).GetComponent<Image>().enabled = false;
                GameObject.Find("PressStart" + ((int)controller.controllerNum + 1)).GetComponent<Image>().enabled = true;
            }
        }
    }
}
