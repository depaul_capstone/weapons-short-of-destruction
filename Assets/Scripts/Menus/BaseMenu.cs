﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseMenu
{
    public enum MenuState
    {
        TitleScreen = 0,
        PlayerSelect = 1,
        MatchOptions = 2,
        TimeOptions = 3,
        ScoreOptions = 4,
        NoMenu = 5,
        InGameMenu = 6,
        MatchResults = 7,
        TitleOptions = 8, // To be better implemented
        InGameOptions = 9 // To be better implemented
    }
    protected MenuState previousState;
    protected MenuState nextState;

    public virtual void Activate(MenuManager manager) { }
    public virtual void Run(MenuManager manager) { }
    public virtual void Deactivate(MenuManager manager) { }

    public virtual void Previous(MenuManager manager) {}
    public virtual void Next(MenuManager manager) {}
}
