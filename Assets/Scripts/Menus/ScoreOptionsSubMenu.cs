﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreOptionsSubMenu : BaseMenu
{
    public override void Activate(MenuManager manager)
    {
        previousState = MenuState.MatchOptions;
        nextState = MenuState.NoMenu;
        manager.ActivateScoreOptionsMenu();
    }
    public override void Run(MenuManager manager)
    {
        manager.UpdateSelectedButton();
        if (Input.GetButtonDown("MenuCancel"))
        {
            Previous(manager);
        }
    }

    public override void Previous(MenuManager manager)
    {
        MenuManager.ChangeState(previousState);
    }
    public override void Next(MenuManager manager)
    {
        MenuManager.ChangeState(nextState);
    }
}
