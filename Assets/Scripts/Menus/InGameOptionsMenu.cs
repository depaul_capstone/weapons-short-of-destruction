﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameOptionsMenu : MainOptionsMenu
{
    public override void Activate(MenuManager manager)
    {
        base.Activate(manager);
        previousState = MenuState.InGameMenu;
    }

    public override void Previous(MenuManager manager)
    {
        base.Previous(manager);
        PlayerManager.UpdateAllPlayerSettings();
    }
}
