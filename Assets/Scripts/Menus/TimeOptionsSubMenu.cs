﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeOptionsSubMenu : BaseMenu
{
    public override void Activate(MenuManager manager)
    {
        previousState = MenuState.MatchOptions;
        nextState = MenuState.NoMenu;
        manager.ActivateTimeOptionsMenu();
    }
    public override void Run(MenuManager manager)
    {
        manager.UpdateSelectedButton();
        if (Input.GetAxis("MenuCancel") > 0)
        {
            Previous(manager);
        }
    }

    public override void Previous(MenuManager manager)
    {
        MenuManager.ChangeState(previousState);
    }
    public override void Next(MenuManager manager)
    {
        MenuManager.ChangeState(nextState);
    }
}
