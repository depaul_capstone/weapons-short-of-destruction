﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeTrap : AnimatedTrap
{
    private float m_playerdeathtime = 1;
    private bool currentlyEating = false;

    protected override void Deactive()
    {
        m_playerdeathtime -= Time.deltaTime;
        if (m_playerdeathtime < 0.0f)
        {
            currentlyEating = false;
            m_animator.SetTrigger("Idle");
            ChangeState(TrapState.Idle);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (currentlyEating == false)
        {
            Player p = other.GetComponent<Player>();
            if (p != null)
            {
                m_animator.SetTrigger("Eat");
                currentlyEating = true;
                m_playerdeathtime = p.deathDelay;
                ChangeState(TrapState.Deactive);
                MatchManager.PlayerDeath(p, this);
            }
        }
    }
}
