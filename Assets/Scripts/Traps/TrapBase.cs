﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class TrapBase : MonoBehaviour
{
    public enum TrapState
    {
        Idle = 0,
        Active = 1,
        Deactive = 2,
        Resetting = 3,
    }
    static private BaseTrapState[] listOfStates = new BaseTrapState[4] { new IdleState(), new ActivateState(), new DeactivateState(), new ResetState() };
    [SerializeField] private TrapState startingState = TrapState.Idle;
    private BaseTrapState currentState;
    [SerializeField] protected List<Camera> listOfDeathCams = new List<Camera>();
    [SerializeField] private int pointsPerKill = 50;
    protected Player m_victim;

    protected virtual void Awake()
    {
        ChangeState(startingState);
        foreach (Camera cam in listOfDeathCams)
        {
            cam.enabled = false;
        }
    }

    protected virtual void Update()
    {
        currentState.run(this);
    }


    public void ChangeState(TrapState state)
    {
        currentState = listOfStates[(int)state];
    }

    public Camera GetRandomDeathCamera()
    {
        int randomCam = (int)(Random.value * listOfDeathCams.Count);
        return listOfDeathCams[randomCam];
    }

    public int GetTrapKillPoints()
    {
        return pointsPerKill;
    }

    protected virtual void Idle() { }
    protected virtual void Active() { }
    protected virtual void Resetting() { }
    protected virtual void Deactive() { }

    public void SetPlayerInTrap(Player p)
    {
        m_victim = p;
    }

    class BaseTrapState
    {
        public virtual void run(TrapBase trap) { }
    }

    class ActivateState : BaseTrapState
    {
        public override void run(TrapBase trap)
        {
            trap.Active();
        }
    }

    class DeactivateState : BaseTrapState
    {
        public override void run(TrapBase trap)
        {
            trap.Deactive();
        }
    }
    class IdleState : BaseTrapState
    {
        public override void run(TrapBase trap)
        {
            trap.Idle();
        }
    }

    class ResetState : BaseTrapState
    {
        public override void run(TrapBase trap)
        {
            trap.Resetting();
        }
    }
}