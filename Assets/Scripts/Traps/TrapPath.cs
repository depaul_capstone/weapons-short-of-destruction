﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TrapPath : MonoBehaviour
{
    private static TrapPathState[] listofStates = new TrapPathState[] { new GetNext(), new GetPrevious(), new GetFirst(), new Stop() };
    private TrapPathState state;
    [SerializeField] private TrapPathTarget[] path;
    private int currentNode;
    private int children;

    private void Start()
    {
        currentNode = 0;
        state = listofStates[3];
        CheckChildren();
    }

    void CheckChildren()
    {
        children = transform.childCount;
        path = GetComponentsInChildren<TrapPathTarget>();
    }

    private void Update()
    {
        if (transform.childCount != children)
        {
            CheckChildren();
        }

        for (int i = 0; i < path.Length - 1; ++i)
        {
            Debug.DrawLine(path[i].transform.position, path[i + 1].transform.position);
        }

        if (path[path.Length - 1].change == TrapPathTarget.TriggerBehaviorChange.LoopToFirst)
        {
            Debug.DrawLine(path[0].transform.position, path[path.Length - 1].transform.position);
        }
    }

    public void ChangeBehavior(TrapPathTarget.TriggerBehaviorChange behavorChange)
    {
        state = listofStates[(int)behavorChange];
    }

    public void ChangeTarget()
    {
        state.run(path.Length, ref currentNode);
    }

    public Vector3 GetTarget()
    {
        return path[currentNode].transform.position;
    }

    abstract class TrapPathState
    {
        public abstract void run(int pathSize, ref int currentNode);
    }
    class GetNext : TrapPathState
    {
        public override void run(int pathSize, ref int currentNode)
        {
            if (++currentNode > pathSize - 1)
            {
                currentNode = pathSize - 1;
            }
        }
    }
    class GetPrevious : TrapPathState
    {
        public override void run(int pathSize, ref int currentNode)
        {
            if (--currentNode < 0)
            {
                currentNode = 0;
            }
        }
    }
    class GetFirst : TrapPathState
    {
        public override void run(int pathSize, ref int currentNode)
        {
            currentNode = 0;
        }
    }
    class Stop : TrapPathState
    {
        public override void run(int pathSize, ref int currentNode)
        {
        }
    }
}
