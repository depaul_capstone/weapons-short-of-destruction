﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffMapTrap : StaticTrap
{
    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p)
        {
            MatchManager.PlayerDeath(p, this);
        }
    }
}
