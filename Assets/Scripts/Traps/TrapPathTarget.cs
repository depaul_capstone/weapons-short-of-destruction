﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TrapPathTarget : MonoBehaviour
{
    public enum TriggerBehaviorChange
    {
        None = -1,
        Frontrack = 0,
        Backtrack = 1,
        LoopToFirst = 2,
        Stop = 3,
    }

    [SerializeField] public TriggerBehaviorChange change = TriggerBehaviorChange.None;
    [SerializeField] public float delay = 0.0f;
    private float timeLeft;
    private TrapPath parent;
    private bool check = true;

    private void Start()
    {
        parent = GetComponentInParent<TrapPath>();
        timeLeft = delay;
    }

    private void OnTriggerStay(Collider other)
    {
        if (check && other.transform.position == transform.position)
        {
            if (timeLeft <= 0.0f)
            {
                if (change != TriggerBehaviorChange.None)
                {
                    parent.ChangeBehavior(change);
                }
                parent.ChangeTarget();
                check = false;
            }
            else
            {
                timeLeft -= Time.deltaTime;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        check = true;
        timeLeft = delay;
    }
}
