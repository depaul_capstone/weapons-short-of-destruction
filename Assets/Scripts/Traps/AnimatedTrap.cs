﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatedTrap : TrapBase
{
    protected Animator m_animator;
    [SerializeField] protected float GlobalAnimationSpeed = 1.0f;

    protected override void Awake()
    {
        base.Awake();
        m_animator = GetComponent<Animator>();
    }

    public void SetGlobalAnimationSpeed(float Speed)
    {
        GlobalAnimationSpeed = 1.0f;
    }
}
