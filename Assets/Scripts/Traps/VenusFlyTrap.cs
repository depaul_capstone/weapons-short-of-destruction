﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusFlyTrap : AnimatedTrap
{
    [SerializeField] private Sudo3DAudioSource sudo3D_src_hiss;
	[SerializeField] private Sudo3DAudioSource sudo3D_src_kill;

    [SerializeField] private PullToTarget[] m_AreaOfInfluence = null;
    private float pullStrength = 1;
    private float m_playerdeathtime = 1;
    private bool currentlyEating = false;

    protected void Start()
    {
		foreach (PullToTarget area in m_AreaOfInfluence) {
			area.SetParentTrap (gameObject);
		}
    }

    public float GetPullStrength()
    {
        return pullStrength;
    }
    protected override void Active()
    {
        foreach (PullToTarget area in m_AreaOfInfluence)
        {
            if (!area.ArePlayersCaught())
            {
                m_animator.SetTrigger("Waiting");
                ChangeState(TrapState.Idle);
                break;
            }
        }
    }

    protected override void Deactive()
    {
        m_playerdeathtime -= Time.deltaTime;
        if (m_playerdeathtime < 0.0f)
        {
            currentlyEating = false;
            m_animator.SetTrigger("Waiting");
            ChangeState(TrapState.Idle);
        }

    }

    protected override void Idle()
    {
        //check to see if anything is in grabbing range
        foreach (PullToTarget area in m_AreaOfInfluence)
        {
            if (area.ArePlayersCaught())
            {
                sudo3D_src_hiss.PlaySingle();
                m_animator.SetTrigger("Opening");
                ChangeState(TrapState.Active);
                break;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (currentlyEating == false)
        {
            Player p = other.GetComponent<Player>();
            if (p != null)
            {
				sudo3D_src_kill.PlaySingle ();
                m_animator.SetTrigger("Eating");
                currentlyEating = true;
                m_playerdeathtime = p.deathDelay;
                foreach (PullToTarget area in m_AreaOfInfluence)
                {
                    area.RemovePlayer(p);
                }
                ChangeState(TrapState.Deactive);
                p.SetRender(false);
                MatchManager.PlayerDeath(p, this);
            }
        }
    }
}
