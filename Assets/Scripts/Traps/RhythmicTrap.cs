﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RhythmicTrap : StaticTrap
{
    [SerializeField] public TrapPath path = null;
    [SerializeField] private float speed = 1.0f;

    // Update is called once per frame
    protected override void Active()
    {
        Debug.DrawLine(transform.position, path.GetTarget(), Color.green);
        transform.position = Vector3.MoveTowards(transform.position, path.GetTarget(), speed * Time.deltaTime);
    }
}
