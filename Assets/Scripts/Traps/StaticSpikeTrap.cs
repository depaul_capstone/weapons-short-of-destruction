﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticSpikeTrap : StaticTrap
{
    private List<Player> activatingObjects = new List<Player>();
    private Sudo3DAudioSource sudoSrc;

    private void Start()
    {
        sudoSrc = this.GetComponent<Sudo3DAudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (other.CompareTag("Player"))
        {
            activatingObjects.Add(p);
            ChangeState(TrapState.Active);
        }
    }

    protected override void Active()
    {
        foreach (Player player in activatingObjects)
        {
            if (!player.GetController().IsImpulseZeroed())
            {
                MatchManager.PlayerDeath(player, this);
                player.PlayAllParticles();
                sudoSrc.PlaySingle();
            }
        }
        activatingObjects.Clear();

        ChangeState(TrapState.Idle);
    }
}
