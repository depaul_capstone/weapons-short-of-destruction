﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerableTrapSwitch : MonoBehaviour
{

    [SerializeField] private List<TriggerableTrap> trapsConnected;
    [SerializeField] private List<string> interactableObjectTags;

    private void OnCollisionEnter(Collision collision)
    {
        if (interactableObjectTags.Contains(collision.gameObject.tag))
        {
            foreach (TriggerableTrap trap in trapsConnected)
            {
                trap.ChangeState(TrapBase.TrapState.Active);
                print(collision.gameObject + " activated a connected trap");
            }
        }
    }
}
