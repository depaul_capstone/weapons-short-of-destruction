﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VineTrap : AnimatedTrap
{
    protected override void Active()
    {
        m_animator.SetTrigger("Attack");
        MatchManager.PlayerDeath(m_victim, this);
        m_victim.PlayAllParticles();
        AnimatorClipInfo[] myAnimatorClip = m_animator.GetNextAnimatorClipInfo(0);
        ChangeState(TrapState.Resetting);
    }
    protected override void Resetting()
    {
        m_animator.SetTrigger("Idle");
        m_victim = null;
        ChangeState(TrapState.Idle);
    }
}
