﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapCollisionCallback : MonoBehaviour
{
    TrapBase parent;

    private void Start()
    {
        parent = transform.parent.GetComponent<TrapBase>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            parent.SetPlayerInTrap(p);
            parent.ChangeState(TrapBase.TrapState.Active);
        }
    }

}
