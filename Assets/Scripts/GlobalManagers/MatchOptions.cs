﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MatchOptions
{

    // Default Match Values
    [SerializeField] int numOfPlayers;
    [SerializeField] CameraManager.CameraOrientation twoPlayerCamOrientation;
    [SerializeField] MatchManager.MatchState endCondition;
    [SerializeField] int lengthOfMatch;
    [SerializeField] int targetEndScore;
    [SerializeField] private List<Color> playerColors; // Currently based on controller number
    [SerializeField] Player.PlayerSettings player1Settings;
    [SerializeField] Player.PlayerSettings player2Settings;
    [SerializeField] Player.PlayerSettings player3Settings;
    [SerializeField] Player.PlayerSettings player4Settings;
    private Player.PlayerSettings[] playerSettings;

    public void Initialize()
    {
        playerSettings = new Player.PlayerSettings[4] { player1Settings, player2Settings, player3Settings, player4Settings };
    }

    public List<Color> GetPlayerColors()
    {
        return playerColors;
    }

    public MatchManager.MatchInfo GetMatchInfo()
    {
        MatchManager.MatchInfo matchInfo;
        matchInfo.numPlayers = numOfPlayers;
        matchInfo.screenOrientation = twoPlayerCamOrientation;
        matchInfo.endCondition = endCondition;
        matchInfo.lengthOfMatch = lengthOfMatch;
        matchInfo.targetScore = targetEndScore;
        matchInfo.player1Settings = playerSettings[0];
        matchInfo.player2Settings = playerSettings[1];
        matchInfo.player3Settings = playerSettings[2];
        matchInfo.player4Settings = playerSettings[3];
        return matchInfo;
    }

    public void SetNumOfPlayers(int num)
    {
        numOfPlayers = num;
    }

    // Horizontal = 0, Vertical = 1
    public void Set2PlayerCamOrientation(CameraManager.CameraOrientation orientation)
    {
        twoPlayerCamOrientation = orientation;
    }

    // Timed = 0, Scored = 1, Endless = 2
    public void SetMatchEndCondition(MatchManager.MatchState endCon)
    {
        endCondition = endCon;
    }

    public void SetLengthOfMatch(int length)
    {
        lengthOfMatch = length;
    }

    public void SetTargetScore(int target)
    {
        targetEndScore = target;
    }

    // Player Settings - Controller Number
    public void SetPlayerControllerNum(int playerNum, int controller)
    {
        playerSettings[playerNum].controllerNum = controller;
        playerSettings[playerNum].playerColor = playerColors[controller];
    }

    // Player Settings - Camera Invert Horizontal
    public void TogglePlayerCamInvertH(int playerNum)
    {
        playerSettings[playerNum].invertCamHorizontal = !playerSettings[playerNum].invertCamHorizontal;
    }

    // Player Settings - Camera Invert Vertical
    public void TogglePlayerCamInvertV(int playerNum)
    {
        playerSettings[playerNum].invertCamVertical = !playerSettings[playerNum].invertCamVertical;
    }

    // Player Settings - Horizontal Sensitivity
    public void SetPlayerHSensitivity(int playerNum, float hSensitivity)
    {
        playerSettings[playerNum].horizontalSensitivity = hSensitivity;
    }

    // Player Settings - Vertical Sensitivity
    public void SetPlayerVSensitivity(int playerNum, float vSensitivity)
    {
        playerSettings[playerNum].verticalSensitivity = vSensitivity;
    }
}
