﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GlobalGameManager : MonoBehaviour
{

    private static GlobalGameManager gameManager = null;
    private static GlobalGameManager GetInstance()
    {
        return gameManager;
    }
    private void Awake()
    {
        if (gameManager != null && gameManager != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            gameManager = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    [SerializeField] private string mainMenuName;
    [SerializeField] private AudioClip menuMusic = null;
    [SerializeField] private string matchSceneName; // Later turn into list and allow players to select from it
    [SerializeField] MatchOptions matchOptions;

    private void Start()
    {
        matchOptions.Initialize();
        SoundManager.PlayMusic(menuMusic, true);
        MenuManager.InitializeMainMenu();
    }

    public static MatchOptions GetMatchOptions()
    {
        return GetInstance().PrivGetMatchOptions();
    }
    private MatchOptions PrivGetMatchOptions()
    {
        return matchOptions;
    }

    // Can also call to restart Match
    public static void StartMatch()
    {
        GetInstance().PrivStartMatch();
    }
    private void PrivStartMatch()
    {
        Debug.Log("Switch Scene to " + matchSceneName);
        SceneManager.LoadScene(matchSceneName);
    }

    public static void InitializeMatch()
    {
        GetInstance().PrivInitializeMatch();
    }
    private void PrivInitializeMatch()
    {
        Debug.Log("Initialize MatchManager");
        MatchManager.Initialize(matchOptions.GetMatchInfo());
        MenuManager.InitializeMatch();
    }

    public static void EndMatch()
    {
        GetInstance().PrivEndMatch();
    }
    private void PrivEndMatch()
    {
        MatchManager.EndMatchCleanUp();
        Debug.Log("Switch Scene to " + mainMenuName);
        SceneManager.LoadScene(mainMenuName);
        SoundManager.PlayMusic(menuMusic, true);
    }
}
