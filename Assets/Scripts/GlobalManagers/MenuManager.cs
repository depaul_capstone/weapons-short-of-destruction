﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using PlayerTubeMaterials = PlayerMenuTube.PlayerTubeMaterials;
using PlayerMaterials = Player.PlayerMaterials;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private AudioClip scrollSnd;
    [SerializeField] [Range(0.0f, 1.0f)] private float scrollVolume;

    [SerializeField] private AudioClip selectSnd;
    [SerializeField] [Range(0.0f, 1.0f)] private float selectVolume;

    [SerializeField] private AudioClip backSnd;
    [SerializeField] [Range(0.0f, 1.0f)] private float backVolume;

    [SerializeField] private AudioSource menuAudioSrc;


    private static MenuManager menuManager = null;
    private static MenuManager GetInstance()
    {
        return menuManager;
    }
    private void Awake()
    {
        if (menuManager != null && menuManager != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            menuManager = this;
        }
        DontDestroyOnLoad(this.gameObject);
        eventSystem = GetComponentInChildren<EventSystem>();
    }

    static private BaseMenu[] listOfMenus =
        new BaseMenu[10] {
            new TitleScreenMenu(),
            new PlayerSelectMenu(),
            new MatchOptionsMenu(),
            new TimeOptionsSubMenu(),
            new ScoreOptionsSubMenu(),
            new InGameNoMenu(),
            new InGamePauseMenu(),
            new MatchResultsMenu(),
            new MainOptionsMenu(),
            new InGameOptionsMenu()
        };
    private BaseMenu currentState;

    [System.Serializable]
    struct MenuSetUp
    {
        public GameObject menuCanvas;
        public GameObject firstSelectedButton;
    }
    [SerializeField] private GameObject staticMenuCanvas;
    [SerializeField] private MenuSetUp titleScreenMenu;
    [SerializeField] private MenuSetUp playerSelectMenu;
    [SerializeField] private MenuSetUp matchOptionsMenu;
    [SerializeField] private MenuSetUp timeOptionsMenu;
    [SerializeField] private MenuSetUp scoreOptionsMenu;
    [SerializeField] private MenuSetUp inGameMenu;
    [SerializeField] private MenuSetUp matchResultsMenu;
    [SerializeField] private MenuSetUp mainOptionsMenu;
    [SerializeField] private MenuSetUp controlOptionsMenu;

    private EventSystem eventSystem;
    private GameObject currentlySelectedButton;
    private Button lastPressedButton = null;

    // Public Methods

    public static BaseMenu GetCurrentMenu()
    {
        return GetInstance().PrivGetCurrentMenu();
    }
    private BaseMenu PrivGetCurrentMenu()
    {
        return currentState;
    }

    public static void InitializeMainMenu()
    {
        GetInstance().PrivInitializeMainMenu();
    }
    private void PrivInitializeMainMenu()
    {
        ChangeState(BaseMenu.MenuState.TitleScreen);
    }

    public static void InitializeMatch()
    {
        GetInstance().PrivInitializeMatch();
    }
    private void PrivInitializeMatch()
    {
        ChangeState(BaseMenu.MenuState.NoMenu);
    }

    public static void NextMenuState()
    {
        GetInstance().PrivNextMenuState();
    }
    private void PrivNextMenuState()
    {
        PlaySelectSnd();
        currentState.Next(this);
    }

    public static void PreviousMenuState()
    {
        GetInstance().PrivPreviousMenuState();
    }
    private void PrivPreviousMenuState()
    {
        PlayBackSnd();
        currentState.Previous(this);
    }

    public static void DeactivatePressedButton(Button button)
    {
        GetInstance().PrivDeactivatePressedButton(button);
    }
    private void PrivDeactivatePressedButton(Button button)
    {
        if (lastPressedButton != null)
        {
            lastPressedButton.interactable = true;
        }
        lastPressedButton = button;
        lastPressedButton.interactable = false;
    }

    public static void GoToMainOptions()
    {
        GetInstance().PrivGoToMainOptions();
    }
    private void PrivGoToMainOptions()
    {
        PlaySelectSnd();
        DeactivateTitleScreenMenu();
        ChangeState(BaseMenu.MenuState.TitleOptions);
    }

    public static void GoToInGameOptions()
    {
        GetInstance().PrivGoToInGameOptions();
    }
    private void PrivGoToInGameOptions()
    {
        PlaySelectSnd();
        DeactivateInGameMenu();
        ChangeState(BaseMenu.MenuState.InGameOptions);
    }

    // "Private" Methods

    private void Update()
    {
        currentState.Run(this);
    }

    public void UpdateSelectedButton()
    {
        if (eventSystem.currentSelectedGameObject != currentlySelectedButton)
        {
            if (eventSystem.currentSelectedGameObject == null)
            {
                eventSystem.SetSelectedGameObject(currentlySelectedButton);
            }
            else
            {
                currentlySelectedButton = eventSystem.currentSelectedGameObject;
                MenuManager.PlayScrollSnd (); 
            }
        }
    }

    public static void ChangeState(BaseMenu.MenuState menu)
    {
        GetInstance().PrivChangeState(menu);
    }
    private void PrivChangeState(BaseMenu.MenuState menu)
    {
        if (lastPressedButton != null)
        {
            lastPressedButton.interactable = true;
            lastPressedButton = null;
        }
        currentState = listOfMenus[(int)menu];
        currentState.Activate(this);
    }

    public void ActivateTitleScreenMenu()
    {
        staticMenuCanvas.SetActive(true);
        titleScreenMenu.menuCanvas.SetActive(true);

        float menuScreenAlpha = 1.0f;
        Material menuScreen2D = GameObject.Find("MenuGlass").GetComponent<MeshRenderer>().material;
        menuScreen2D.SetColor("_Color", new Color(menuScreen2D.color.r, menuScreen2D.color.g, menuScreen2D.color.b, menuScreenAlpha));

        currentlySelectedButton = titleScreenMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);
    }
    public void DeactivateTitleScreenMenu()
    {
        titleScreenMenu.menuCanvas.SetActive(false);
    }

    public void ActivatePlayerSelectMenu()
    {
        playerSelectMenu.menuCanvas.SetActive(true);
        GameObject.Find("Players Ready").GetComponent<Button>().interactable = false;
        GameObject.Find("PressA1").GetComponent<Image>().enabled = true;
        GameObject.Find("PressA2").GetComponent<Image>().enabled = true;
        GameObject.Find("PressA3").GetComponent<Image>().enabled = true;
        GameObject.Find("PressA4").GetComponent<Image>().enabled = true;
        GameObject.Find("Ready1").GetComponent<Image>().enabled = false;
        GameObject.Find("Ready2").GetComponent<Image>().enabled = false;
        GameObject.Find("Ready3").GetComponent<Image>().enabled = false;
        GameObject.Find("Ready4").GetComponent<Image>().enabled = false;
        
        GameObject playerRobot;
        GameObject playerTube;
        for (int i = 0; i < 4; ++i)
        {
            int playerIndexOffset = i + 1;
            playerTube = GameObject.Find("CharacterTube" + playerIndexOffset);
            playerTube.GetComponentInChildren<MeshRenderer>().materials[(int)PlayerTubeMaterials.TubeStripe].SetColor("_Color", GlobalGameManager.GetMatchOptions().GetPlayerColors()[i]);
            playerRobot = GameObject.Find("Player" + playerIndexOffset);
            playerRobot.GetComponentInChildren<SkinnedMeshRenderer>().materials[(int)PlayerMaterials.PlayerStripes].SetColor("_Color", GlobalGameManager.GetMatchOptions().GetPlayerColors()[i]);
            if (!playerTube.GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleTop"))
            {
                // Invisibly Raising Robot
                playerRobot.GetComponentInChildren<Animator>().SetTrigger("Jumping");
                playerRobot.GetComponent<MenuRobotAnimations>().EnableRobotMesh(false);
                playerRobot.GetComponent<MenuRobotAnimations>().ChangeState(MenuRobotAnimations.MenuRobotState.GoingUp);

                // Lifting Tube
                playerTube.GetComponentInChildren<Animator>().SetTrigger("Raise");
            }
            else if (playerRobot.GetComponent<MenuRobotAnimations>().GetCurrentState() != MenuRobotAnimations.MenuRobotState.Up)
            {
                // Invisibly Raising Robot - Needed when returning to MainMenu scene from another scene
                playerRobot.GetComponent<MenuRobotAnimations>().EnableRobotMesh(false);
                playerRobot.GetComponent<MenuRobotAnimations>().ChangeState(MenuRobotAnimations.MenuRobotState.GoingUp);
            }
        }

        Material menuScreen2D = GameObject.Find("MenuGlass").GetComponent<MeshRenderer>().material;
        menuScreen2D.SetColor("_Color", new Color(menuScreen2D.color.r, menuScreen2D.color.g, menuScreen2D.color.b, 0));

        currentlySelectedButton = playerSelectMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);
    }
    public void DeactivatePlayerSelectMenu()
    {
        playerSelectMenu.menuCanvas.SetActive(false);
    }

    public void ActivateMatchOptionsMenu()
    {
        matchOptionsMenu.menuCanvas.SetActive(true);
        GameObject.Find("Start Match").GetComponent<Button>().interactable = false;

        GameObject playerTube;
        for (int i = 0; i < 4; ++i)
        {
            int playerIndexOffset = i + 1;
            playerTube = GameObject.Find("CharacterTube" + playerIndexOffset);
            if (!playerTube.GetComponentInChildren<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleBottom"))
            {
                playerTube.GetComponentInChildren<Animator>().SetTrigger("Lower");
            }
        }

        float menuScreenAlpha = 0.75f;
        Material menuScreen2D = GameObject.Find("MenuGlass").GetComponent<MeshRenderer>().material;
        menuScreen2D.SetColor("_Color", new Color(menuScreen2D.color.r, menuScreen2D.color.g, menuScreen2D.color.b, menuScreenAlpha));

        currentlySelectedButton = matchOptionsMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);

        GameObject.Find("Timed Game").GetComponent<Button>().interactable = true;
        GameObject.Find("Scored Game").GetComponent<Button>().interactable = true;
        DeactivateTimeOptionsMenu();
        DeactivateScoreOptionsMenu();
    }
    public void DeactivateMatchOptionsMenu()
    {
        matchOptionsMenu.menuCanvas.SetActive(false);
    }

    public void ActivateTimeOptionsMenu()
    {
        GameObject.Find("Timed Game").GetComponent<Button>().interactable = false;
        GameObject.Find("Scored Game").GetComponent<Button>().interactable = false;
        foreach (Button timeButton in timeOptionsMenu.menuCanvas.GetComponentsInChildren<Button>())
        {
            timeButton.interactable = true;
        }

        currentlySelectedButton = timeOptionsMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);
    }
    public void DeactivateTimeOptionsMenu()
    {
        foreach (Button timeButton in timeOptionsMenu.menuCanvas.GetComponentsInChildren<Button>())
        {
            timeButton.interactable = false;
            timeButton.GetComponent<SelectableButtons>().UseAltDisabledSprite(false);
        }
    }

    public void ActivateScoreOptionsMenu()
    {
        GameObject.Find("Timed Game").GetComponent<Button>().interactable = false;
        GameObject.Find("Scored Game").GetComponent<Button>().interactable = false;
        foreach (Button scoreButton in scoreOptionsMenu.menuCanvas.GetComponentsInChildren<Button>())
        {
            scoreButton.interactable = true;
        }

        currentlySelectedButton = scoreOptionsMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);
    }
    public void DeactivateScoreOptionsMenu()
    {
        foreach (Button scoreButton in scoreOptionsMenu.menuCanvas.GetComponentsInChildren<Button>())
        {
            scoreButton.interactable = false;
            scoreButton.GetComponent<SelectableButtons>().UseAltDisabledSprite(false);
        }
    }

    public void ActivateInGameMenu()
    {
        inGameMenu.menuCanvas.SetActive(true);

        currentlySelectedButton = inGameMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);
    }
    public void DeactivateInGameMenu()
    {
        inGameMenu.menuCanvas.SetActive(false);
    }

    public void ActivateMatchResultsMenu()
    {
        matchResultsMenu.menuCanvas.SetActive(true);
        for (int num = 1; num <= 4; ++num)
        {
            GameObject.Find("P" + num).GetComponent<Image>().enabled = false;
            GameObject.Find("P" + num + "Wins").GetComponent<Image>().enabled = false;
        }

        currentlySelectedButton = matchResultsMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);
    }
    public void DeactivateMatchResultsMenu()
    {
        matchResultsMenu.menuCanvas.SetActive(false);
    }

    public void ActivateMainOptionsMenu()
    {
        mainOptionsMenu.menuCanvas.SetActive(true);

        currentlySelectedButton = mainOptionsMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);
    }
    public void DeactivateMainOptionsMenu()
    {
        mainOptionsMenu.menuCanvas.SetActive(false);
    }

    public void ActivateControlOptionsMenu()
    {
        controlOptionsMenu.menuCanvas.SetActive(true);

        currentlySelectedButton = controlOptionsMenu.firstSelectedButton;
        eventSystem.SetSelectedGameObject(currentlySelectedButton);
    }
    public void DeactivateControlOptionsMenu()
    {
        controlOptionsMenu.menuCanvas.SetActive(false);
    }

    public void RemoveAllMenus()
    {
        staticMenuCanvas.SetActive(false);
        DeactivateTitleScreenMenu();
        DeactivatePlayerSelectMenu();
        DeactivateMatchOptionsMenu();
        DeactivateInGameMenu();
        DeactivateMatchResultsMenu();
    }


    // Audio
    public static void PlayScrollSnd() { GetInstance().PrivPlayScrollSnd(); }
    private void PrivPlayScrollSnd()
    {
        menuAudioSrc.volume = scrollVolume;
        SoundManager.PlaySingle(scrollSnd, menuAudioSrc);
    }

    public static void PlaySelectSnd() { GetInstance().PrivPlaySelectSnd(); }
    private void PrivPlaySelectSnd()
    {
        menuAudioSrc.volume = selectVolume;
        SoundManager.PlaySingle(selectSnd, menuAudioSrc);
    }

    public static void PlayBackSnd() { GetInstance().PrivPlayBackSnd(); }
    private void PrivPlayBackSnd()
    {
        menuAudioSrc.volume = backVolume;
        SoundManager.PlaySingle(backSnd, menuAudioSrc);
    }

}
