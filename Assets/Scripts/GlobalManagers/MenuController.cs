﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    private MatchOptions matchOptions;

    private void Start()
    {
        matchOptions = GlobalGameManager.GetMatchOptions();
    }

    // Can also restart match
    public void StartMatch()
    {
        GlobalGameManager.StartMatch();
    }

    // Will not work in the editor besides debug log
    public void QuitGame()
    {
        Application.Quit();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }

    public void DeactivateButton(Button button)
    {
        MenuManager.DeactivatePressedButton(button);
    }

    public void DeactivateAllMenus()
    {
        MenuManager.InitializeMatch();
    }

    public void GoToMainOptions()
    {
        MenuManager.PlaySelectSnd();
        MenuManager.GoToMainOptions();
    }

    public void GoToInGameOptions()
    {
        MenuManager.PlaySelectSnd();
        MenuManager.GoToInGameOptions();
    }

    public void NextState()
    {
        MenuManager.PlaySelectSnd();
        MenuManager.NextMenuState();
    }

    public void PreviousState()
    {
        MenuManager.PlayBackSnd();
        MenuManager.PreviousMenuState();
    }

    // Match Settings

    // Horizontal = 0, Vertical = 1
    public void Set2PlayerCamOrientation(int orientation)
    {
        matchOptions.Set2PlayerCamOrientation((CameraManager.CameraOrientation)orientation);
    }

    // Timed = 0, Scored = 1, Endless = 2
    public void SetMatchEndCondition(int endCondition)
    {
        MatchManager.MatchState desiredMatchState = (MatchManager.MatchState)endCondition;
        matchOptions.SetMatchEndCondition(desiredMatchState);
        if (desiredMatchState != MatchManager.MatchState.Endless)
        {
            if (desiredMatchState == MatchManager.MatchState.Scored)
            {
                MenuManager.ChangeState(BaseMenu.MenuState.ScoreOptions);
            }
            else if (desiredMatchState == MatchManager.MatchState.Timed)
            {
                MenuManager.ChangeState(BaseMenu.MenuState.TimeOptions);
            }
        }
        else
        {

            GameObject.Find("Start Match").GetComponent<Button>().interactable = true;
        }
        MenuManager.PlaySelectSnd();
    }

    public void SetLengthOfMatch(int length)
    {
        matchOptions.SetLengthOfMatch(length);
        GameObject.Find("Start Match").GetComponent<Button>().interactable = true;
        MenuManager.PlaySelectSnd();
    }

    public void SetTargetScore(int target)
    {
        matchOptions.SetTargetScore(target);
        GameObject.Find("Start Match").GetComponent<Button>().interactable = true;
        MenuManager.PlaySelectSnd();
    }

    // Player Settings - Camera Invert
    public void TogglePlayerCamInvertH()
    {
        matchOptions.TogglePlayerCamInvertH(GetPlayerMenuNum());
        MenuManager.PlaySelectSnd ();
    }
    public void TogglePlayerCamInvertV()
    {
        matchOptions.TogglePlayerCamInvertV(GetPlayerMenuNum());
        MenuManager.PlaySelectSnd ();
    }

    // Player Settings - Horizontal Sensitivity
    public void SetPlayerHSensitivity(float hSensitivity)
    {
        matchOptions.SetPlayerHSensitivity(GetPlayerMenuNum(), hSensitivity);
        MenuManager.PlaySelectSnd();
    }

    // Player Settings - Vertical Sensitivity
    public void SetPlayerVSensitivity(float vSensitivity)
    {
        matchOptions.SetPlayerVSensitivity(GetPlayerMenuNum(), vSensitivity);
        MenuManager.PlaySelectSnd();
    }


    // Sound Options

    public void SetGameSoundEffectsVolume(Slider volumeSlider)
    {
        SoundManager.SetGameSoundEffectsVolume(volumeSlider.value);
    }

    public void SetGameMusicVolume(Slider volumeSlider)
    {
        SoundManager.SetGameMusicVolume(volumeSlider.value);
    }

    public void SetAnnouncerVolume(Slider volumeSlider)
    {
        Announcer.SetAnnouncerVolume(volumeSlider.value);
    }


    // Private Methods

    private int GetPlayerMenuNum()
    {
        MainOptionsMenu optionsMenu = MenuManager.GetCurrentMenu() as MainOptionsMenu;
        if (optionsMenu != null)
        {
            return optionsMenu.GetPlayerMenuNum();
        }
        return -1;
    }
}
