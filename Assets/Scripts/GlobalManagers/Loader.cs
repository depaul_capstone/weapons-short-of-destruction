﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour
{

    public GameObject gameMasterManager;
    public GameObject soundManager;
    public GameObject xboxControllerManager;
    public GameObject menuManager;
    public GameObject announcer;
    // Add prefabs of other global managers here


    void Awake()
    {
        Instantiate(gameMasterManager);
        Instantiate(soundManager);
        Instantiate(xboxControllerManager);
        Instantiate(menuManager);
        Instantiate(announcer);

        // Instantiate other global managers here
    }
}
