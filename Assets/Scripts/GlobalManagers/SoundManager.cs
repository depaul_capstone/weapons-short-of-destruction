﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
public class SoundManager : MonoBehaviour
{
    private static SoundManager instance = null;

    [SerializeField] private ListenerManager listenerManager;
    [SerializeField] private AudioSource efxSource = null;
    [SerializeField] private AudioSource musicSource = null;
    [SerializeField] private float lowPitchRange = .95f;
    [SerializeField] private float highPitchRange = 1.05f;
    private static SoundManager GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        listenerManager.UpdateSources();
    }

    //INTERFACE ============================================
    public static void RegisterListener(SudoListener listener_)
    {
        Assert.IsNotNull(listener_);
        GetInstance().PrivateRegisterListener(listener_);
    }
    public static void DeregisterListener(SudoListener listener_)
    {
        Assert.IsNotNull(listener_);
        GetInstance().PrivateDeregisterListener(listener_);
    }

    public static void Register3DAudioSource(Sudo3DAudioSource source_)
    {
        Assert.IsNotNull(source_);
        GetInstance().PrivateRegister3DAudioSource(source_);
    }

    public static void Deregister3DAudioSource(Sudo3DAudioSource source_)
    {
        Assert.IsNotNull(source_);
        GetInstance().PrivateDeregister3DAudioSource(source_);
    }

    // PlayMusic Takes in a song to play on the default audio source
    public static void PlayMusic(AudioClip song_, bool loopSong_ = false)
    {
        Assert.IsNotNull(song_, "PlayMusic has a null ref to AudioClip");
        GetInstance().PrivPlayMusic(song_, loopSong_);
    }

    // PlayMusic takes in a song and a source to play it on 
    public static void PlayMusic(AudioClip song_, AudioSource source_, bool loopSong_ = false)
    {
        Assert.IsNotNull(song_, "PlayMusic has a null ref to AudioClip");
        Assert.IsNotNull(source_, "PlayMusic has a null ref to AudioSource");

        source_.clip = song_;
        source_.loop = loopSong_;
        source_.Play();
    }

    public static void PlaySingle(AudioClip clip_)
    {
        Assert.IsNotNull(clip_, "PlaySingle has a null ref to AudioClip");
        GetInstance().PrivPlaySingle(clip_);
    }

    public static void PlaySingle(AudioClip clip_, AudioSource source_)
    {
        Assert.IsNotNull(clip_, "PlaySingle has a null ref to AudioClip");
        Assert.IsNotNull(source_, "PlaySingle has a null ref to AudioSource");
        GetInstance().PrivPlaySingle(clip_, source_);
    }
    public static void PlaySingle3D(AudioClip clip_, AudioSource source_)
    {
        Assert.IsNotNull(clip_, "PlaySingle has a null ref to AudioClip");
        Assert.IsNotNull(source_, "PlaySingle has a null ref to AudioSource");

        source_.clip = clip_;
        source_.Play();
    }

    public static void RandomizeSfx(params AudioClip[] clips_)
    {
        Assert.IsNotNull(clips_, "RandomizeSFX has a null ref to AudioClip");
        GetInstance().PrivRandomizeSfx(clips_);
    }

    public static void SetGameSoundEffectsVolume(float volume)
    {
        GetInstance().PrivSetGameSoundEffectsVolume(volume);
    }
    public static float GetGameSoundEffectsVolume()
    {
        return GetInstance().PrivGetGameSoundEffectsVolume();
    }

    public static void SetGameMusicVolume(float volume)
    {
        GetInstance().PrivSetGameMusicVolume(volume);
    }
    public static float GetGameMusicVolume()
    {
        return GetInstance().PrivGetGameMusicVolume();
    }


    //PRIVATES ============================================
    private void PrivateRegisterListener(SudoListener listener_)
    {
        listenerManager.RegisterListener(listener_);
    }
    private void PrivateDeregisterListener(SudoListener listener_)
    {
        listenerManager.DeregisterListener(listener_);
    }

    private void PrivateRegister3DAudioSource(Sudo3DAudioSource source_)
    {
        listenerManager.Register3DAudioSource(source_);
    }

    private void PrivateDeregister3DAudioSource(Sudo3DAudioSource source_)
    {
        listenerManager.Deregister3DAudioSource(source_);
    }

    private void PrivPlayMusic(AudioClip song_, bool loopSong_)
    {
        if (musicSource.clip == null || musicSource.clip != song_)
        {
            musicSource.clip = song_;
            musicSource.loop = loopSong_;
            musicSource.Play();
        }

    }

    private void PrivPlaySingle(AudioClip clip_)
    {
        //if (!efxSource.isPlaying)
       // {
            efxSource.clip = clip_;
            efxSource.Play();
        //}
    }
    private void PrivPlaySingle(AudioClip clip_, AudioSource source_)
    {
        source_.clip = clip_;
        source_.Play();
    }

    private void PrivRandomizeSfx(params AudioClip[] clips_)
    {
        int randomIndex = Random.Range(0, clips_.Length);
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        efxSource.pitch = randomPitch;
        efxSource.clip = clips_[randomIndex];
        efxSource.Play();
    }

    private void PrivSetGameSoundEffectsVolume(float volume)
    {
        efxSource.volume = volume;
    }
    private float PrivGetGameSoundEffectsVolume()
    {
        return efxSource.volume;
    }

    private void PrivSetGameMusicVolume(float volume)
    {
        musicSource.volume = volume;
    }
    private float PrivGetGameMusicVolume()
    {
        return musicSource.volume;
    }
}
