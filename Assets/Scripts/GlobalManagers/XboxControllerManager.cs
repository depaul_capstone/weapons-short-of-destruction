﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class XboxControllerManager : MonoBehaviour
{
    private static XboxControllerManager instance = null;
    private static XboxControllerManager GetInstance()
    {
        return instance;
    }
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
        UpdateAllGamePadStates();
    }

    private void Update()
    {
        UpdateAllGamePadStates();
    }

    private void UpdateAllGamePadStates()
    {
        foreach (Joystick j in gamepads)
        {
            j.UpdateState();
        }
    }

    public enum button
    {
        A, B, X, Y, Start, Back, XboxIcon, LeftBumper, RightBumper, LeftJoystick, RightJoystick
    }
    public enum axis
    {
        LeftJoyStick, RightJoyStick, LeftTrigger, RightTrigger
    }

    private Joystick[] gamepads = new Joystick[] { new Joystick(0), new Joystick(1), new Joystick(2), new Joystick(3) };
    private Joystick.Buttons[] buttons = new Joystick.Buttons[] { new Joystick.A(), new Joystick.B(), new Joystick.X(), new Joystick.Y(), new Joystick.Start(),
                                           new Joystick.Back(), new Joystick.XboxIcon(), new Joystick.LeftBumper(), new Joystick.RightBumper(),
                                           new Joystick.LeftJoyStickButton(), new Joystick.RightJoyStickButton() };
    private Joystick.Axis[] axes = new Joystick.Axis[] { new Joystick.LeftJoyStickAxis(), new Joystick.RightJoyStickAxis(), new Joystick.LeftTrigger(), new Joystick.RightTrigger() };


    public static bool IsConnected(PlayerIndex JoystickNum)
    {
        return GetInstance().PrivIsConnected(JoystickNum);
    }

    private bool PrivIsConnected(PlayerIndex joystickNum)
    {
        return gamepads[(int)joystickNum].isConnected();
    }

    public static bool GetButtonPress(PlayerIndex JoystickNum, button b)
    {
        return GetInstance().PrivGetButtonPress(JoystickNum, b);
    }
    public static bool GetButtonHold(PlayerIndex JoystickNum, button b)
    {
        return GetInstance().PrivGetButtonHold(JoystickNum, b);
    }
    public static bool GetButtonRelease(PlayerIndex JoystickNum, button b)
    {
        return GetInstance().PrivGetButtonRelease(JoystickNum, b);
    }

    public static float GetAxisHorizontal(PlayerIndex JoystickNum, axis a)
    {
        return GetInstance().PrivGetAxisHorizontal(JoystickNum, a);
    }
    public static float GetAxisVertical(PlayerIndex JoystickNum, axis a)
    {
        return GetInstance().PrivGetAxisVertical(JoystickNum, a);
    }

    private bool PrivGetButtonPress(PlayerIndex JoystickNum, button b)
    {
        if (gamepads[(int)JoystickNum].isConnected())
        {
            return buttons[(int)b].Press(gamepads[(int)JoystickNum]);
        }
        else
        {
            return false;
        }
    }
    private bool PrivGetButtonHold(PlayerIndex JoystickNum, button b)
    {
        if (gamepads[(int)JoystickNum].isConnected())
        {
            return buttons[(int)b].Hold(gamepads[(int)JoystickNum]);
        }
        else
        {
            return false;
        }
    }
    private bool PrivGetButtonRelease(PlayerIndex JoystickNum, button b)
    {
        if (gamepads[(int)JoystickNum].isConnected())
        {
            return buttons[(int)b].Release(gamepads[(int)JoystickNum]);
        }
        else
        {
            return false;
        }
    }

    private float PrivGetAxisHorizontal(PlayerIndex JoystickNum, axis a)
    {
        if (gamepads[(int)JoystickNum].isConnected())
        {
            return axes[(int)a].Horizontal(gamepads[(int)JoystickNum]);
        }
        else
        {
            return 0.0f;
        }
    }
    private float PrivGetAxisVertical(PlayerIndex JoystickNum, axis a)
    {
        if (gamepads[(int)JoystickNum].isConnected())
        {
            return axes[(int)a].Vertical(gamepads[(int)JoystickNum]);
        }
        else
        {
            return 0.0f;
        }
    }

    public static void SetVibration(PlayerIndex JoystickNum, float leftMotor, float rightMotor)
    {
        GetInstance().PrivSetVibration(JoystickNum, leftMotor, rightMotor);
    }

    private void PrivSetVibration(PlayerIndex joystickNum, float leftMotor, float rightMotor)
    {
        gamepads[(int)joystickNum].SetVibration(leftMotor, rightMotor);
    }

    [System.Serializable]
    public class Joystick
    {
        PlayerIndex playerIndex;
        GamePadState state;
        GamePadState prevState;

        public bool isConnected()
        {
            return state.IsConnected;
        }

        public void SetVibration(float leftMotor, float rightMotor)
        {
            GamePad.SetVibration(playerIndex, leftMotor, rightMotor);
        }

        public void UpdateState()
        {
            prevState = state;
            state = GamePad.GetState(playerIndex);
        }

        public Joystick(int playernum)
        {
            playerIndex = (PlayerIndex)playernum;
        }

        public abstract class Axis
        {
            public abstract float Horizontal(Joystick j);
            public abstract float Vertical(Joystick j);
        }
        public class LeftJoyStickAxis : Axis
        {
            public override float Horizontal(Joystick j)
            {
                return j.state.ThumbSticks.Left.X;
            }
            public override float Vertical(Joystick j)
            {
                return j.state.ThumbSticks.Left.Y;

            }
        }
        public class RightJoyStickAxis : Axis
        {
            public override float Horizontal(Joystick j)
            {
                return j.state.ThumbSticks.Right.X;
            }
            public override float Vertical(Joystick j)
            {
                return j.state.ThumbSticks.Right.Y;

            }
        }
        public class RightTrigger : Axis
        {
            public override float Horizontal(Joystick j)
            {
                return Vertical(j);
            }
            public override float Vertical(Joystick j)
            {
                return j.state.Triggers.Right;
            }
        }
        public class LeftTrigger : Axis
        {
            public override float Horizontal(Joystick j)
            {
                return Vertical(j);
            }
            public override float Vertical(Joystick j)
            {
                return j.state.Triggers.Left;
            }
        }

        public abstract class Buttons
        {
            public abstract bool Press(Joystick j);
            public abstract bool Release(Joystick j);
            public abstract bool Hold(Joystick j);
        }
        public class A : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.A == ButtonState.Released && j.state.Buttons.A == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.A == ButtonState.Pressed && j.state.Buttons.A == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.A == ButtonState.Pressed && j.state.Buttons.A == ButtonState.Pressed) ? true : false;
            }
        }
        public class B : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.B == ButtonState.Released && j.state.Buttons.B == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.B == ButtonState.Pressed && j.state.Buttons.B == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.B == ButtonState.Pressed && j.state.Buttons.B == ButtonState.Pressed) ? true : false;
            }
        }
        public class X : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.X == ButtonState.Released && j.state.Buttons.X == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.X == ButtonState.Pressed && j.state.Buttons.X == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.X == ButtonState.Pressed && j.state.Buttons.X == ButtonState.Pressed) ? true : false;
            }
        }
        public class Y : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.Y == ButtonState.Released && j.state.Buttons.Y == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.Y == ButtonState.Pressed && j.state.Buttons.Y == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.Y == ButtonState.Pressed && j.state.Buttons.Y == ButtonState.Pressed) ? true : false;
            }
        }
        public class Start : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.Start == ButtonState.Released && j.state.Buttons.Start == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.Start == ButtonState.Pressed && j.state.Buttons.Start == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.Start == ButtonState.Pressed && j.state.Buttons.Start == ButtonState.Pressed) ? true : false;
            }
        }
        public class Back : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.Back == ButtonState.Released && j.state.Buttons.Back == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.Back == ButtonState.Pressed && j.state.Buttons.Back == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.Back == ButtonState.Pressed && j.state.Buttons.Back == ButtonState.Pressed) ? true : false;
            }
        }
        public class XboxIcon : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.Guide == ButtonState.Released && j.state.Buttons.Guide == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.Guide == ButtonState.Pressed && j.state.Buttons.Guide == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.Guide == ButtonState.Pressed && j.state.Buttons.Guide == ButtonState.Pressed) ? true : false;
            }
        }
        public class LeftBumper : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.LeftShoulder == ButtonState.Released && j.state.Buttons.LeftShoulder == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.LeftShoulder == ButtonState.Pressed && j.state.Buttons.LeftShoulder == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.LeftShoulder == ButtonState.Pressed && j.state.Buttons.LeftShoulder == ButtonState.Pressed) ? true : false;
            }
        }
        public class RightBumper : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.RightShoulder == ButtonState.Released && j.state.Buttons.RightShoulder == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.RightShoulder == ButtonState.Pressed && j.state.Buttons.RightShoulder == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.RightShoulder == ButtonState.Pressed && j.state.Buttons.RightShoulder == ButtonState.Pressed) ? true : false;
            }
        }
        public class LeftJoyStickButton : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.LeftStick == ButtonState.Released && j.state.Buttons.LeftStick == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.LeftStick == ButtonState.Pressed && j.state.Buttons.LeftStick == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.LeftStick == ButtonState.Pressed && j.state.Buttons.LeftStick == ButtonState.Pressed) ? true : false;
            }
        }
        public class RightJoyStickButton : Buttons
        {
            public override bool Press(Joystick j)
            {
                return (j.prevState.Buttons.RightStick == ButtonState.Released && j.state.Buttons.RightStick == ButtonState.Pressed) ? true : false;
            }
            public override bool Release(Joystick j)
            {
                return (j.prevState.Buttons.RightStick == ButtonState.Pressed && j.state.Buttons.RightStick == ButtonState.Released) ? true : false;
            }
            public override bool Hold(Joystick j)
            {
                return (j.prevState.Buttons.RightStick == ButtonState.Pressed && j.state.Buttons.RightStick == ButtonState.Pressed) ? true : false;
            }
        }
    }
}
