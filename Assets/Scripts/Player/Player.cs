﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
public class Player : MonoBehaviour
{
    public enum PlayerMaterials
    {
        PlayerStripes = 1
    }

    public enum PlayerState
    {
        Alive = 0,
        Dead = 1,
        Spawning = 2
    }
    static private BasePlayerState[] listOfStates = new BasePlayerState[3] { new AliveState(), new DeadState(), new RespawnState() };
    private PlayerState currentState;

    public enum PlayerSensitivityPresets
    {
        Least = 1,
        Some = 2,
        Middle = 4,
        More = 6,
        Most = 8
    }

    [System.Serializable]
    public struct PlayerSettings
    {
        public int controllerNum;
        public bool invertCamHorizontal;
        public bool invertCamVertical;
        public float horizontalSensitivity;
        public float verticalSensitivity;
        public Color playerColor; // Currently based on controller number
    }
    private int playerNum;
    private Camera fpCamera;
    [SerializeField] private Transform WeaponPosition;
    [SerializeField] public float deathDelay = 5.0f;
    [SerializeField] private GameObject deathParticles;
    private float timeLeftDead;
    private Player lastPlayerToHit;
    private int playerScore = 0;
    [SerializeField] private WeaponHandler handler = new WeaponHandler();

    // Sounds
    [SerializeField] private AudioClip playerDeathSnd;
    [SerializeField] [Range(0, 1)] private float deathVol;
    [SerializeField] private AudioSource deathSrc;


    public Robot2PlayerController GetController()
    {
        return GetComponent<Robot2PlayerController>();
    }

    public void Initialize(int playerNum, PlayerSettings playerSettings, Transform spawnTransform)
    {
        print("Player Created!");
        this.playerNum = playerNum;
        UpdatePlayerSettings(playerSettings);
        fpCamera = GetComponentInChildren<Camera>();
        handler.SetPlayer(this);
        Spawn(spawnTransform);
    }

    public void UpdatePlayerSettings(PlayerSettings playerSettings)
    {
        Robot2PlayerController controller = GetController();
        controller.SetControllerNumber(playerSettings.controllerNum);
        controller.SetCameraInvertX(playerSettings.invertCamHorizontal);
        controller.SetCameraInvertY(playerSettings.invertCamVertical);
        controller.SetXSensitivity(playerSettings.horizontalSensitivity);
        controller.SetYSensitivity(playerSettings.verticalSensitivity);
        GetComponentInChildren<SkinnedMeshRenderer>().materials[(int)PlayerMaterials.PlayerStripes].SetColor("_Color", playerSettings.playerColor);
    }

    public void ResetPlayer(Transform spawnTransform)
    {
        playerScore = 0;
        Spawn(spawnTransform);
    }

    public Transform getWeaponPosition()
    {
        return WeaponPosition;
    }

    public Animator GetAnimator()
    {
        return GetController().GetAnimator();
    }

    public void SetWeapon(WeaponBase weapon)
    {
        handler.SwitchWeapon(weapon);
        GetAnimator().SetBool("HasGun", true);
    }
    public void DisarmPlayer()
    {
        handler.SetWeaponToNull();
    }

    public void SetRender(bool state)
    {
        Renderer[] all = GetComponentsInChildren<Renderer>();
        foreach (Renderer item in all)
        {
            item.enabled = state;
        }
    }

    private void Spawn(Transform spawnTransform)
    {
        print("Player Spawned!");
        SetRender(true);
        CameraManager.SetActiveCamera(playerNum, fpCamera);
        transform.SetPositionAndRotation(spawnTransform.position, spawnTransform.localRotation);
        handler.SetWeaponToNull();
        GetController().ZeroImpulse();
        // Call SoundManager for respawn noise
        // Trigger animation and/or undo ragdolling
        GetAnimator().SetTrigger("Spawning");
        ChangeState(PlayerState.Alive);
        ResetLastPlayerToHit();
        GetController().enabled = true;
    }

    private void Alive()
    {
    }

    public void PlayAllParticles()
    {
        Instantiate(deathParticles, transform.position + new Vector3(0, 1, 0), transform.rotation);
    }

    private void Dead()
    {
        Robot2PlayerController controller = GetController();

        if (controller.enabled)
        {
            deathSrc.volume = deathVol;
            SoundManager.PlaySingle(playerDeathSnd, deathSrc);
            controller.enabled = false;
            handler.SetWeaponToNull();
            Debug.Log("Player Death!");
            timeLeftDead = deathDelay;
            // Call SoundManager for death noise
            // Trigger animation and/or ragdolling
            GetAnimator().SetTrigger("Dying");
        }

        if (timeLeftDead <= 0.0f)
        {
            ChangeState(PlayerState.Spawning);
        }
        else
        {
            timeLeftDead -= Time.deltaTime;
        }
    }
    public Reticle GetReticle()
    {
        return CameraManager.GetActiveHUD(GetPlayerNum()).GetComponentInChildren<Reticle>();
    }
    public int GetPlayerNum()
    {
        return playerNum;
    }
    public void ApplyImpulseToPlayer(Vector3 hit, Player playerWhoHit)
    {
        GetController().ApplyImpulse(hit, hit.magnitude);
        if (playerWhoHit != null)
        {
            lastPlayerToHit = playerWhoHit;
        }
    }
    public void ZeroImpulseOnPlayer()
    {
        GetController().ZeroImpulse();
    }

    public void ResetLastPlayerToHit()
    {
        lastPlayerToHit = null;
    }

    public Player GetLastPlayerToHit()
    {
        if (GetController().IsImpulseZeroed())
        {
            return null;
        }
        return lastPlayerToHit;
    }

    public void AddToPlayerScore(int points)
    {
        playerScore += points;
    }

    public int GetPlayerScore()
    {
        return playerScore;
    }

    protected void Update()
    {
        listOfStates[(int)currentState].run(this);
    }

    public void ChangeState(PlayerState state)
    {
        currentState = state;
    }

    public PlayerState GetCurrentState()
    {
        return currentState;
    }

    class BasePlayerState
    {
        public virtual void run(Player player) { }
    }

    class AliveState : BasePlayerState
    {
        public override void run(Player player)
        {
            player.Alive();
        }
    }

    class DeadState : BasePlayerState
    {
        public override void run(Player player)
        {
            player.Dead();
        }
    }
    class RespawnState : BasePlayerState
    {
        public override void run(Player player)
        {
            player.Spawn(PlayerManager.GetSpawnLocation());
        }
    }
}
