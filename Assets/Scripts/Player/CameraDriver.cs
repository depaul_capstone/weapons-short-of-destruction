﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDriver : MonoBehaviour
{
    [SerializeField] private Transform Spine;
    [SerializeField] private Transform Eyes;

    void LateUpdate()
    {
        Spine.localRotation *= Quaternion.Inverse(transform.localRotation);
        transform.position = Eyes.position;
    }
}
