﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPositionDriver : MonoBehaviour
{
    [SerializeField] private Transform DominantHand;
    private void Awake()
    {
        transform.SetParent(DominantHand);
    }
}
