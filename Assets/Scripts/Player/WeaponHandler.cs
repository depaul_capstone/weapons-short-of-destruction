﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class WeaponHandler
{
    [SerializeField] private WeaponBase nullWeapon;
    private WeaponBase currentWeapon;
    private Player player;

    public void SetWeaponToNull()
    {
        SwitchWeapon(nullWeapon);
        player.GetAnimator().SetBool("HasGun", false);
    }
    public void SetPlayer(Player player)
    {
        this.player = player;
    }
    public Player GetPlayer()
    {
        return player;
    }
    private void RemoveWeapon()
    {
        if (currentWeapon != null)
        {
            currentWeapon.EndProcesses();
            MonoBehaviour.Destroy(currentWeapon.gameObject);
        }
    }
    public void SwitchWeapon(WeaponBase newWeapon)
    {
        RemoveWeapon();
        AcquireWeapon(newWeapon);
    }
    public void AcquireWeapon(WeaponBase newWeapon)
    {
        GameObject weapon = MonoBehaviour.Instantiate(newWeapon.gameObject);
        currentWeapon = weapon.GetComponent<WeaponBase>();
        Transform point = GetPlayer().getWeaponPosition();
        currentWeapon.Initialize(this, point);
    }
}
