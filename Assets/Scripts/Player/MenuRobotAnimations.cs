﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuRobotAnimations : MonoBehaviour
{
    [SerializeField] AnimationCurve liftingRobotYPos;
    [SerializeField] AnimationCurve droppingRobotYPos;

    public enum MenuRobotState
    {
        Up = 0,
        GoingDown = 1,
        Down = 2,
        GoingUp = 3
    }
    static private BaseMenuRobotState[] listOfStates = new BaseMenuRobotState[4] { new UpState(), new GoingDownState(), new DownState(), new GoingUpState() };
    private MenuRobotState currentState = MenuRobotState.Up;
    private float stateStart = 0;

    public void EnableRobotMesh(bool isEnabled)
    {
        GetComponentInChildren<SkinnedMeshRenderer>().enabled = isEnabled;
        GetComponentsInChildren<MeshRenderer>()[0].enabled = isEnabled;
        GetComponentsInChildren<MeshRenderer>()[1].enabled = isEnabled;
    }

    public MenuRobotState GetCurrentState()
    {
        return currentState;
    }

    public void ChangeState(MenuRobotState menuRobotState)
    {
        currentState = menuRobotState;
        stateStart = Time.time;
    }
	
	private void Update ()
    {
        listOfStates[(int)currentState].run(this);
	}

    private void Up()
    {

    }

    private void GoingDown()
    {
        float newRobotHeight;
        float currentStateLength = Time.time - stateStart;
        if (currentStateLength > droppingRobotYPos.keys[droppingRobotYPos.length - 1].time)
        {
            newRobotHeight = droppingRobotYPos.Evaluate(droppingRobotYPos.keys[droppingRobotYPos.length - 1].time);
            ChangeState(MenuRobotState.Down);
            GetComponentInChildren<Animator>().SetTrigger("Landing");
        }
        else
        {
            newRobotHeight = droppingRobotYPos.Evaluate(currentStateLength);
        }
        
        transform.localPosition = new Vector3(transform.localPosition.x, newRobotHeight, transform.localPosition.z);
    }

    private void Down()
    {

    }

    private void GoingUp()
    {
        float newRobotHeight;
        float currentStateLength = Time.time - stateStart;
        if (currentStateLength > Time.time + liftingRobotYPos.keys[liftingRobotYPos.length - 1].time)
        {
            newRobotHeight = liftingRobotYPos.Evaluate(liftingRobotYPos.keys[liftingRobotYPos.length - 1].time);
            ChangeState(MenuRobotState.Up);
        }
        else
        {
            newRobotHeight = liftingRobotYPos.Evaluate(currentStateLength);
        }

        transform.localPosition = new Vector3(transform.localPosition.x, newRobotHeight, transform.localPosition.z);
    }

    class BaseMenuRobotState
    {
        public virtual void run(MenuRobotAnimations menuRobot) { }
    }

    class UpState : BaseMenuRobotState
    {
        public override void run(MenuRobotAnimations menuRobot)
        {
            menuRobot.Up();
        }
    }
    class GoingDownState : BaseMenuRobotState
    {
        public override void run(MenuRobotAnimations menuRobot)
        {
            menuRobot.GoingDown();
        }
    }
    class DownState : BaseMenuRobotState
    {
        public override void run(MenuRobotAnimations menuRobot)
        {
            menuRobot.Down();
        }
    }
    class GoingUpState : BaseMenuRobotState
    {
        public override void run(MenuRobotAnimations menuRobot)
        {
            menuRobot.GoingUp();
        }
    }
}
