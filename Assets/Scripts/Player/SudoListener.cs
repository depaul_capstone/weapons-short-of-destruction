﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SudoListener : MonoBehaviour
{

    [SerializeField] private float maxDist;

    public float GetNormalizedDistanceFromSource(AudioSource audioSource_)
    {
        // find the distance
        float dist = Vector3.Distance(audioSource_.transform.position, this.transform.position);
        float normalizedDist = dist / maxDist;

        // note: normalizedDist can be > 1.0f - this would result in the sound not being played 
        // for this listener because the object is too far away. 
        return normalizedDist;
    }
    public float CalculateVolume(AudioSource audioSource_)
    {
        // find the distance
        float normalizedDist = Vector3.Distance(audioSource_.transform.position, this.transform.position) / maxDist;

        // if listener in range of sound
        if (normalizedDist < 1.0f)
        {
            // max volume (1) - a normalized value of dist / maxDist to get resulting volume between 0-1
            return 1.0f - normalizedDist;
        }
        else
        {
            // If too far away volume = 0
            return 0.0f;
        }
    }

    public float CalculatePan(AudioSource audioSource_)
    {
        // find vector between listener and source
        Vector3 vectA = audioSource_.transform.position - this.transform.position;

        // find source forward vector
        Vector3 vectB = this.transform.right;

        // normalize the vectors 
        vectA.Normalize();
        vectB.Normalize();

        // find cos theta between the two
        float cosTheta = Vector3.Dot(vectB, vectA) / (Vector3.Magnitude(vectB) * Vector3.Magnitude(vectA));

        // debug
        Debug.DrawRay(this.transform.position, vectA);
        Debug.DrawRay(this.transform.position, vectB);

        return cosTheta;
    }
}
