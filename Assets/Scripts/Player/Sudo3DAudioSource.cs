﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sudo3DAudioSource : MonoBehaviour
{
    [SerializeField] private AudioClip clip;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] [Range(0, 1)] private float maxVolume;

    private void Start()
    {
        SoundManager.Register3DAudioSource(this);
    }

    public void PlaySingle()
    {
        SoundManager.PlaySingle3D(clip, audioSource);
    }

    public float GetMaxVolume() { return maxVolume; }
    public AudioSource GetAudioSource() { return audioSource; }
    public void SetVolume(float vol) { audioSource.volume = vol; }
    public void SetPan(float pan) { audioSource.panStereo = pan; }

	void OnDisable()
	{
		SoundManager.Deregister3DAudioSource (this); 
	}
}
