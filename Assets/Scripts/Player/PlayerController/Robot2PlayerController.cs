﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Robot2PlayerController : MonoBehaviour
{
    private static bool IsKeyboardInUse = false;
    private CharacterController m_controller = null;
    private Animator m_animator = null;
    private Vector3 MovementVector = Vector3.zero;
    private Vector3 ImpulseVector = Vector3.zero;
    private Vector3 AccelerationVector = Vector3.zero;
    private Vector3 JumpVector = Vector3.zero;
    private Vector3 GravityVector = Vector3.zero;
    //private PlayerInputScheme m_Layout = null;
    private PlayerJumpState m_jumpstate = new PlayerJumpState();
    private Quaternion m_CharacterTargetRot = Quaternion.identity;
    private Quaternion m_CameraTargetRot = Quaternion.identity;
    private Camera m_FPCam = null;
    //staticSelectors
    private static PlayerInputScheme[] listOfInputSchemes = new PlayerInputScheme[] { new KeyBoardLayout(), new XboxLayout(), new DisabledLayout() };
    //User customizable
    [SerializeField] private float Mass = 62;
    [SerializeField] private float movmentSpeed = 5f;
    [SerializeField] private float GravityMultiplier = 1.5f;
    [SerializeField] private float jumpForce = 2000f;
    [SerializeField] private bool cameraInvertY = false;
    [SerializeField] private float ySensitivity = 1.0f;
    [SerializeField] private bool cameraInvertX = false;
    [SerializeField] private float xSensitivity = 1.0f;
    [SerializeField] private LayoutType controlType = LayoutType.Xbox;
    [SerializeField] private XInputDotNetPure.PlayerIndex ControllerNumber = XInputDotNetPure.PlayerIndex.One;

    // Sounds
    [SerializeField] private AudioSource runSrc;
    [SerializeField] private AudioSource jumpSrc;

    [SerializeField] private AudioClip jumpSnd;
    [SerializeField] [Range(0.0f, 1.0f)] private float jumpVol;
    [SerializeField] private AudioClip landingSnd;
    [SerializeField] [Range(0.0f, 1.0f)] private float landVol;
    [SerializeField] private AudioClip runSnd;
    [SerializeField] [Range(0.0f, 1.0f)] private float runVol;





    private void Awake()
    {
        m_controller = GetComponent<CharacterController>();
        m_animator = GetComponentInChildren<Animator>();
        m_FPCam = GetComponentInChildren<Camera>();
        m_CharacterTargetRot = transform.localRotation;
        m_CameraTargetRot = m_FPCam.transform.localRotation;
        m_jumpstate.SetState(State.Falling);
    }
    private void Start()
    {
        KeyboardSwitch();
    }

    private void FixedUpdate()
    {
        MovementVector = Vector3.zero;
        //Player Input
        MovementVector += (transform.right * listOfInputSchemes[(int)controlType].MovementHorizontal(ControllerNumber)) * movmentSpeed;
        MovementVector += (transform.forward * listOfInputSchemes[(int)controlType].MovementVertical(ControllerNumber)) * movmentSpeed;
        bool isMoving = System.Math.Abs(MovementVector.x) > 0 || System.Math.Abs(MovementVector.z) > 0;
        m_animator.SetBool("IsMoving", isMoving);

        //Play Running sound if player is moving and on the ground 
        PlayRunningSound(isMoving);

        //Add Force Vectors
        MovementVector += GravityVector;
        MovementVector += JumpVector;
        MovementVector += ImpulseVector;
        MovementVector += AccelerationVector;

        //reduce the Deminishing Vectors
        ImpulseVector = Vector3.Lerp(ImpulseVector, Vector3.zero, 5 * Time.deltaTime);
        JumpVector = Vector3.Lerp(JumpVector, Vector3.zero, 5 * Time.deltaTime);

        //Update Camera
        cameraRotation();

        //Move
        MovementVector *= Time.deltaTime;
        m_controller.Move(MovementVector);
    }

    private void LateUpdate()
    {
        //UpdateStates
        m_jumpstate.RunState(this);
    }

    private void PlayRunningSound(bool isMoving)
    {
        if (isMoving && m_controller.isGrounded)
        {
            if (!runSrc.isPlaying)
            {
                runSrc.volume = runVol;
                SoundManager.PlaySingle(runSnd, runSrc);
            }
        }
        else
        {
            runSrc.Stop();
        }
    }

    private void OnDisable()
    {
        controlType = LayoutType.Disabled;
    }

    private void OnEnable()
    {
        KeyboardSwitch();
    }
    private void KeyboardSwitch()
    {
        if (ControllerNumber == XInputDotNetPure.PlayerIndex.One)
        {
            if (IsKeyboardInUse != false || listOfInputSchemes[(int)controlType].IsControllerConnected(ControllerNumber) == false)
            {
                controlType = LayoutType.Keyboard;
                IsKeyboardInUse = true;
            }
            else
            {
                controlType = LayoutType.Xbox;
                IsKeyboardInUse = false;
            }
        }
        else
        {
            controlType = LayoutType.Xbox;
        }
    }

    public void SetControllerVibration(float leftMotor, float rightMotor)
    {
        listOfInputSchemes[(int)controlType].SetVibration(ControllerNumber, leftMotor, rightMotor);
    }
    public void SetControllerVibration(float bothMotor)
    {
        listOfInputSchemes[(int)controlType].SetVibration(ControllerNumber, bothMotor, bothMotor);
    }
    public void SetControllerNumber(int num)
    {
        ControllerNumber = (XInputDotNetPure.PlayerIndex)num;
    }
    public void SetCameraInvertX(bool invert)
    {
        cameraInvertX = invert;
    }
    public void SetCameraInvertY(bool invert)
    {
        cameraInvertY = invert;
    }
    public void SetXSensitivity(float sensitivity)
    {
        if (sensitivity != 0)
        {
            xSensitivity = sensitivity;
        }
    }
    public void SetYSensitivity(float sensitivity)
    {
        if (sensitivity != 0)
        {
            ySensitivity = sensitivity;
        }
    }

    public float GetFireInput()
    {
        return listOfInputSchemes[(int)controlType].GetFire(ControllerNumber);
    }
    public float GetJumpInput()
    {
        return listOfInputSchemes[(int)controlType].GetJump(ControllerNumber);
    }
    public void ApplyImpulse(Vector3 dir, float force)
    {
        dir.Normalize();
        ImpulseVector += dir * force / Mass;
    }
    public bool IsImpulseZeroed()
    {
        return ImpulseVector == Vector3.zero;
    }
    public void Jump(Vector3 dir, float force)
    {
        jumpSrc.volume = jumpVol;
        SoundManager.PlaySingle(jumpSnd, jumpSrc);
        dir.Normalize();
        JumpVector += dir * force / Mass;
    }

    public void AddGravityAcceleration(Vector3 dir)
    {
        GravityVector += (dir * Time.deltaTime);
    }
    public void ApplyAcceleration(Vector3 dir)
    {
        AccelerationVector += (dir * Time.deltaTime);
    }
    public void ZeroImpulse()
    {
        ImpulseVector = Vector3.zero;
    }
    public void ZeroGravity()
    {
        GravityVector = Vector3.zero;
    }
    public Animator GetAnimator()
    {
        return m_animator;
    }
    void cameraRotation()
    {

        float yRot = cameraInvertX ? -1 : 1;
        yRot *= listOfInputSchemes[(int)controlType].LookHorizontal(ControllerNumber) * xSensitivity;
        float xRot = cameraInvertY ? -1 : 1;
        xRot *= listOfInputSchemes[(int)controlType].LookVertical(ControllerNumber) * ySensitivity;

        m_CharacterTargetRot *= Quaternion.Euler(0f, yRot, 0f);
        m_CameraTargetRot *= Quaternion.Euler(-xRot, 0f, 0f);

        m_CameraTargetRot = ClampRotationAroundXAxis(m_CameraTargetRot);

        transform.localRotation = m_CharacterTargetRot;
        m_FPCam.transform.localRotation = m_CameraTargetRot;
    }
    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, -90, 90);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
    public enum LayoutType
    {
        Keyboard = 0,
        Xbox = 1,
        Disabled = 2,
    }
    public enum State
    {
        Grounded = 0,
        Jumping = 1,
        Falling = 2,
        Buffer = 3,
    }
    public interface PlayerInputScheme
    {
        bool IsControllerConnected(XInputDotNetPure.PlayerIndex playerNum);
        float MovementVertical(XInputDotNetPure.PlayerIndex playerNum);
        float MovementHorizontal(XInputDotNetPure.PlayerIndex playerNum);
        float LookVertical(XInputDotNetPure.PlayerIndex playerNum);
        float LookHorizontal(XInputDotNetPure.PlayerIndex playerNum);
        float GetJump(XInputDotNetPure.PlayerIndex playerNum);
        float GetFire(XInputDotNetPure.PlayerIndex playerNum);
        void SetVibration(XInputDotNetPure.PlayerIndex playerNum, float leftMotor, float rightMotor);
    }

    class DisabledLayout : PlayerInputScheme
    {
        public bool IsControllerConnected(XInputDotNetPure.PlayerIndex playerNum) { return XboxControllerManager.IsConnected(playerNum); }
        public float MovementVertical(XInputDotNetPure.PlayerIndex playerNum) { return 0.0f; }
        public float MovementHorizontal(XInputDotNetPure.PlayerIndex playerNum) { return 0.0f; }
        public float LookVertical(XInputDotNetPure.PlayerIndex playerNum) { return 0.0f; }
        public float LookHorizontal(XInputDotNetPure.PlayerIndex playerNum) { return 0.0f; }
        public float GetJump(XInputDotNetPure.PlayerIndex playerNum) { return 0.0f; }
        public float GetFire(XInputDotNetPure.PlayerIndex playerNum) { return 0.0f; }
        public void SetVibration(XInputDotNetPure.PlayerIndex playerNum, float leftMotor, float rightMotor) {}

    }

    class XboxLayout : PlayerInputScheme
    {
        public float MovementHorizontal(XInputDotNetPure.PlayerIndex playerNum)
        {
            return XboxControllerManager.GetAxisHorizontal(playerNum, XboxControllerManager.axis.LeftJoyStick);
        }
        public float MovementVertical(XInputDotNetPure.PlayerIndex playerNum)
        {
            return XboxControllerManager.GetAxisVertical(playerNum, XboxControllerManager.axis.LeftJoyStick);
        }
        public float GetJump(XInputDotNetPure.PlayerIndex playerNum)
        {
            return XboxControllerManager.GetButtonHold(playerNum, XboxControllerManager.button.A) ? 1 : 0;
        }
        public float GetFire(XInputDotNetPure.PlayerIndex playerNum)
        {
            return XboxControllerManager.GetAxisVertical(playerNum, XboxControllerManager.axis.RightTrigger);
        }
        public float LookVertical(XInputDotNetPure.PlayerIndex playerNum)
        {
            return XboxControllerManager.GetAxisVertical(playerNum, XboxControllerManager.axis.RightJoyStick);
        }

        public float LookHorizontal(XInputDotNetPure.PlayerIndex playerNum)
        {
            return XboxControllerManager.GetAxisHorizontal(playerNum, XboxControllerManager.axis.RightJoyStick);
        }

        public bool IsControllerConnected(XInputDotNetPure.PlayerIndex playerNum)
        {
            return XboxControllerManager.IsConnected(playerNum);
        }
        public void SetVibration(XInputDotNetPure.PlayerIndex playerNum, float leftMotor, float rightMotor)
        {
            XboxControllerManager.SetVibration(playerNum, leftMotor, rightMotor);
        }

    }
    class KeyBoardLayout : PlayerInputScheme
    {
        public bool IsControllerConnected(XInputDotNetPure.PlayerIndex playerNum)
        {
            return XboxControllerManager.IsConnected(playerNum);
        }
        public float MovementHorizontal(XInputDotNetPure.PlayerIndex playerNum)
        {
            return Input.GetAxisRaw("Horizontal1");
        }
        public float MovementVertical(XInputDotNetPure.PlayerIndex playerNum)
        {
            return Input.GetAxisRaw("Vertical1");
        }
        public float GetJump(XInputDotNetPure.PlayerIndex playerNum)
        {
            return Input.GetAxis("Jump1");
        }
        public float GetFire(XInputDotNetPure.PlayerIndex playerNum)
        {
            return Input.GetAxis("Fire1");
        }

        public float LookVertical(XInputDotNetPure.PlayerIndex playerNum)
        {
            return Input.GetAxis("VerticalTurn1");
        }

        public float LookHorizontal(XInputDotNetPure.PlayerIndex playerNum)
        {
            return Input.GetAxis("HorizontalTurn1");
        }
        public void SetVibration(XInputDotNetPure.PlayerIndex playerNum, float leftMotor, float rightMotor)
        {
        }
    }

    public class PlayerJumpState
    {
        public static PlayerVerticalState[] listOfVerticalStates = new PlayerVerticalState[] { new GroundedState(), new JumpingState(), new FallingState(), new BufferState() };
        private PlayerVerticalState currentInstance = null;
        public void RunState(Robot2PlayerController player) { currentInstance.Trigger(player); }
        public void SetState(State s) { currentInstance = listOfVerticalStates[(int)s]; }
        public State GetState() { return currentInstance.type; }
    }

    public abstract class PlayerVerticalState
    {
        public State type;
        public virtual void Trigger(Robot2PlayerController player) { }
    }

    public class GroundedState : PlayerVerticalState
    {
        public GroundedState()
        {
            type = State.Grounded;
        }
        public override void Trigger(Robot2PlayerController player)
        {
            if (!player.m_controller.isGrounded)
            {
                player.m_jumpstate.SetState(State.Buffer);
            }
            if (player.GetJumpInput() > 0)
            {
                player.Jump(player.transform.up, player.jumpForce);
                player.m_animator.SetTrigger("Jumping");
                player.m_jumpstate.SetState(State.Jumping);
            }
        }
    }
    public class JumpingState : PlayerVerticalState
    {
        public JumpingState()
        {
            type = State.Jumping;
        }
        public override void Trigger(Robot2PlayerController player)
        {
            if (player.JumpVector.magnitude <= Physics.gravity.magnitude * player.GravityMultiplier)
            {
                player.m_jumpstate.SetState(State.Falling);
            }
        }
    }
    public class FallingState : PlayerVerticalState
    {
        public FallingState()
        {
            type = State.Falling;
        }

        public override void Trigger(Robot2PlayerController player)
        {
            if (!player.m_controller.isGrounded)
            {
                player.AddGravityAcceleration(Physics.gravity * player.GravityMultiplier);
            }
            else
            {
                player.jumpSrc.volume = player.landVol;
                SoundManager.PlaySingle(player.landingSnd, player.jumpSrc);
                player.m_animator.SetTrigger("Landing");
                player.ZeroGravity();
                player.AddGravityAcceleration(Physics.gravity * player.GravityMultiplier);
                player.m_jumpstate.SetState(State.Grounded);
            }
        }
    }
    public class BufferState : PlayerVerticalState
    {
        [SerializeField] private int BufferTime = 15; //frames
        private int TimeLeft = 0;

        public BufferState()
        {
            type = State.Buffer;
            TimeLeft = BufferTime;
        }
        public override void Trigger(Robot2PlayerController player)
        {
            if (TimeLeft > 0)
            {
                if (player.GetJumpInput() > 0)
                {
                    player.ZeroGravity();
                    player.Jump(player.transform.up, player.jumpForce);
                    player.m_animator.SetTrigger("Jumping");
                    player.m_jumpstate.SetState(State.Jumping);
                    TimeLeft = BufferTime;
                }
                else
                {
                    player.AddGravityAcceleration(Physics.gravity * player.GravityMultiplier);
                    TimeLeft--;
                }
            }
            else
            {
                player.m_jumpstate.SetState(State.Falling);
                TimeLeft = BufferTime;
            }
        }
    }
}